import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

//import com.thoughtworks.selenium.DefaultSelenium;

public class Calculator {

	public static void main(String[] args) {
		/*FirefoxDriver fdriver = new FirefoxDriver();
		fdriver.get("https://accounts.google.com");*/
		
		// for chrome and ie need to use system property tag, but firefox driver no need on  2.53.1.jar
		System.setProperty("webdriver.chrome.driver",
				"C:\\Selenium_Prerequisite\\chromedriver\\chromedriver_win32\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://accounts.google.com");
		System.out.println(driver.getTitle());
		driver.close();
		driver.quit();
		
		// upto 2.53.1 version support seleniumRC, means deadultSelenium class is available
		//DefaultSelenium dfs = new DefaultSelenium();
		

	}

}
