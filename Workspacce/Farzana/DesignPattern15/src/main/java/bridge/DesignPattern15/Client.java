package bridge.DesignPattern15;
//Create a BridgePatternDemo or client class.

public class Client {
	//It enables the separation of implementation from the interface.
	//It improves the extensibility.
	//It allows the hiding of implementation details from the client.
	//When you don't want a permanent binding between the functional abstraction and its implementation.

	public static void main(String[] args) {
		QuestionFormat questions = new QuestionFormat("Java Programming Language");
		questions.q = new JavaQuestions();
		questions.delete("what is class?");
		questions.display();
		questions.newOne("What is inheritance? ");

		questions.newOne("How many types of inheritance are there in java?");
		questions.displayAll();
	}

}
