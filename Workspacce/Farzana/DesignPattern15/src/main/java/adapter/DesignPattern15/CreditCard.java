package adapter.DesignPattern15;
//(Target interface)
public interface CreditCard {
	 public void giveBankDetails();  
	    public String getCreditCard();  
	// End of the CreditCard interface.  
}
