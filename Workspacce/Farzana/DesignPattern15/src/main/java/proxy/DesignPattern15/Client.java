package proxy.DesignPattern15;
// proxy means an object representing another object.
//provides the control for accessing the original object".
//It provides the protection to the original object from the outside world.
//So, we can perform many operations like hiding the information of original object, 
//on demand loading etc.

//Now, Create a ProxyPatternClient class that can access the internet actually.
public class Client {

	public static void main(String[] args) {
		OfficeInternetAccess access = new ProxyInternetAccess("Ashwani Rajput");  
	        access.grantInternetAccess();  
	}

}
