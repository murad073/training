package proxy.DesignPattern15;
//Create an OfficeInternetAccess interface.

public interface OfficeInternetAccess {
	public void grantInternetAccess();
}
