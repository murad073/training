package Exercise.class19;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws InterruptedException
    {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\UserDB\\Documents\\Software\\chromedriver\\chromedriver_win32\\chromedriver.exe");
    	WebDriver driver = new ChromeDriver();
    	
    	// implicit wait is for webElement
    
    	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    	
    	// page load timeout
    	
    	driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
    	
    	
    	
    	// Options x = driver.manage();
    	// Timeouts t = x.timeouts();
    	// t.implicitlyWait(15, TimeUnit.SECONDS);
    	
    	driver.navigate().to("https://www.indeed.com/");
    	driver.manage().window().maximize();
    	
    	WebElement whatTextBox = driver.findElement(By.id("text-input-what"));
    	WebElement findJobButton = driver.findElement(By.xpath("//button[.='Find Jobs']"));
    	
    	whatTextBox.sendKeys("java selenium");
    	findJobButton.click();
    	
    	//driver,findElement
    	
    	Thread.sleep(5000); // 5000 milisecond a 5 second hoy // explicitlyWait
    	
    	driver.quit();
    }
}
