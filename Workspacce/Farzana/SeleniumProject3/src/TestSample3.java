import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class TestSample3 {

	public static void main(String[] args) {
		/*System.setProperty("webdriver.gecko.driver", "C:\\Selenium_Prerequisite\\FirefoxDriver\\geckodriver-v0.22.0-win64\\geckodriver.exe");
		FirefoxDriver driver = new FirefoxDriver();*/
		
		/*System.setProperty("webdriver.chrome.driver",
				"C:\\Selenium_Prerequisite\\chromedriver\\chromedriver_win32\\chromedriver.exe");*/
		// ChromeDriver driver = new ChromeDriver();
		System.setProperty("webdriver.ie.driver","C:\\Selenium_Prerequisite\\IEDriver\\IEDriverServer_Win32_3.14.0\\IEDriverServer.exe");
		@SuppressWarnings("deprecation")
		InternetExplorerDriver driver = new InternetExplorerDriver(capabilities);
		
		DesiredCapabilities capabilities  = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		
		
		
		
		//System.setProperty("webdriver.ie.driver","C:\\Selenium_Prerequisite\\IEDriver\\IEDriverServer_Win32_3.14.0\\IEDriverServer.exe");
		//InternetExplorerDriver driver = new InternetExplorerDriver();
		
		// upto 2.53.1 version support seleniumRC, means deadultSelenium class is available
				// from selenium3 version no more support seleniumRC, means deadultSelenium class is not available
		//DefaultSelenium dfs = new DefaultSelenium();
		
		driver.get("https://accounts.google.com");
		System.out.println(driver.getTitle());
		//driver.close();
		//driver.quit();

	}

}
