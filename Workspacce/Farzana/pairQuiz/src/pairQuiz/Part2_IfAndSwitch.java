package pairQuiz;

public class Part2_IfAndSwitch {

	public static void main(String[] args) {
		
		boolean momAtHome = false;
		boolean isRaining = true;
		boolean javaSelenium18 = false;
		boolean homeWork = true;
		 
	      if (momAtHome) {
	    	  if (isRaining) {
	          System.out.println("Let's enjoy vuna khichuri today");
	          }
	    	  else {
	    	  System.out.println("Let's go out for shopping");	     
	    	  }
	      }
	      else if (javaSelenium18) {
	          if (homeWork){
	          System.out.println("We have to finish homework before sunday");
	          } 
	      	  else {
	    	  System.out.println("Yes... no homework for this week");	     
	    	  }
	      }
	      else {
	      System.out.println("Why mom is not at home, I want to eat vuna khichuri");
	      }
	      System.out.println("How was it?");
	      
	      /// ternary operator, it uses 3 operands. can be used to replace if...else statement
	      
	      boolean isFemale = true;
	      String gender;
	      
	      gender = (isFemale) ? "yes, I m female" : "I m male" ;
	      System.out.println(gender);
	      
	      if (isFemale) {
		      System.out.println("Females are the best");
	      }
	      else {
		      System.out.println("Males are not good");
	      }
	      
	      // switch statement. can a substitute for long if..else..if ladders
	      
	      int inClassStudent = 9;
	      String Name;
	      
	      switch (inClassStudent) {
	    	  case 1:
	    		  Name = "Farzana";
	    		 break;
	    	  case 2:
	    		  Name = "Khudeza";
	    		 break;
	    	  case 3:
	    		  Name = "Mimi";
	    		  break;
	    	  case 4:
	    		  Name = "Munira";
	    		  break;
	    	  case 5:
	    		  Name = "Nazrul";
	    		  break;
	    	  case 6:
	    		  Name = "Nishat";
	    		  break;
	    	  case 7:
	    		  Name = "Shoeb";
	    		  break;
	    	  case 8:
	    	  	  Name = "Tuli";
    		      break;  
    		  default:
    			  Name = "Invalid name or not in class student";
    		      break;  	  
	      }
	      System.out.println(Name);
	      
	      
	      /* import java.util.Scanner;

    class Calculator {
    public static void main(String[] args) {

    	char operator;
    	Double number1, number2, result;
    	
    	Scanner scanner = new Scanner(System.in);
    	System.out.print("Enter operator (either +, -, * or /): ");
    	operator = scanner.next().charAt(0);
    	System.out.print("Enter number1 and number2 respectively: ");
    	number1 = scanner.nextDouble();
    	number2 = scanner.nextDouble();
    	
    	switch (operator) {
         case '+':
           result = number1 + number2;
    	   System.out.print(number1 + "+" + number2 + " = " + result);
           break;

         case '-':
           result = number1 - number2;
           System.out.print(number1 + "-" + number2 + " = " + result);
           break;

         case '*':
           result = number1 * number2;
           System.out.print(number1 + "*" + number2 + " = " + result);
           break;

         case '/':
           result = number1 / number2;
           System.out.print(number1 + "/" + number2 + " = " + result);
           break;

         default: 
           System.out.println("Invalid operator!");
           break;
        }       
    }
}

	       * 
	       */

	      
	    		  

	    
	      
	}
}


	



