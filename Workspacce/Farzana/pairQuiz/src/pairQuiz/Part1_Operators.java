package pairQuiz;

public class Part1_Operators {

	public static void main(String[] args) {
		
		int num1 = 20;
	    int num2 = 5;
	    System.out.println("num1 / num2: " + (num1 / num2) + " and " + "num1 % num2: " +             
	                         (num1 % num2));
	    num2 = num1;
	    num1 *= num2;
	    System.out.println("Now num2 value is " + num2  + " and num1 value is " + num1);

	    num1++;
	    num2--;
	    System.out.println("Now num1 value is " + num1 + " and " + "num2 value is " 
	                        +num2);

	    boolean b1 = num1 > num2;
	    boolean b2 = false;
	    System.out.println("Boolean value  b1 is " + b1 + " and " + "b2 is " + b2);
	    System.out.println("Boolean value (b1 && b2) is " + (b1 && b2) + " and " + "(b1 || b2) is " 
                             + (b2 || b1) + " and " + "!(b1 && b2) is " + !(b1 && b2));
	    
	    boolean b3 = num1 == num2;
	    boolean b4 = num2 <= num1;
	    System.out.println("Boolean value  b3 is " + b3 + " and " + "b4 is " + b4);
	    

	}

}
