import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

//import com.thoughtworks.selenium.DefaultSelenium;

public class TestSample1 {

	public static void main(String[] args) {
		// need to have firefox version 47.0.1 or 47.0.2 no later then this to work with jar version 2.53.1
		// from 3.3 jar verison need the latest version of firefox
		
		//FirefoxDriver driver = new FirefoxDriver();
		
		
		// for chrome and ie need to use system property tag, but firefox driver no need mention system propertyon  2.53.1.jar
		/*System.setProperty("webdriver.chrome.driver",
				"C:\\Selenium_Prerequisite\\chromedriver\\chromedriver_win32\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();*/
		
		
		
		DesiredCapabilities capabilities  = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		
		/*capabilities.setCapability("nativeEvents", false);
		capabilities.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
		capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);*/
				
		
		System.setProperty("webdriver.ie.driver","C:\\Selenium_Prerequisite\\IEDriver\\IEDriverServer_Win32_3.14.0\\IEDriverServer.exe");
		InternetExplorerDriver driver = new InternetExplorerDriver(capabilities);
		
		driver.get("https://accounts.google.com/signin");
		System.out.println(driver.getTitle());
		//driver.close();
		//driver.quit();
		
		// upto 2.53.1 version support seleniumRC, means deadultSelenium class is available
		//DefaultSelenium dfs = new DefaultSelenium();
		

	}

}
