import java.util.Scanner;

public class ScannerTest {

	public static void main(String[] args) {
		String name;
		int age;

		Scanner input = new Scanner(System.in);

		System.out.println("Please type your name: ");
		// take input from user
		name = input.nextLine();

		System.out.println("What is your age? ");
		// take input from user
		age = input.nextInt();

		System.out.println("Hello " + name + ". You are " + age + " years of old.");

	}

}
