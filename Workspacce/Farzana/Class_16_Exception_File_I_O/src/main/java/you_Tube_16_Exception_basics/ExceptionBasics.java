package you_Tube_16_Exception_basics;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class ExceptionBasics {
	public static void main(String[] args) {
		int i = 0;
		boolean done = false;
		do {
			try {
				Scanner input = new Scanner(System.in);
				System.out.print("Enter an Integer : ");
				i = input.nextInt();

				//done = true;
				System.out.printf( "Good Job!... You entered Integer %d\n", i);
			} catch (InputMismatchException e) {
				System.out.println("You did not enter an Integer");
			} finally {
				System.out.println("This happens whether the exception happens or not");

			}
			//System.out.printf("You entered %d\n", i);

		} while (!done);

	}
}
