package you_Tube_16_ObjectSerialization;

import java.io.Serializable;

public class Student implements Serializable {
	// declaring variables or members in private
	private String name;
	private double grade;
	
	
	// to set students name and grade, creating constructor
	public Student (String name, double d) {
		this.grade = d;
		this.name = name;
	}
	
	// declare 2 get method
	
	
	public String getName() {
		return this.name;
	}
	
	public double getGrade() {
		return this.grade;
	}
	
	public double setGrade(double newGrade) {
		return (this.grade=newGrade);
	}

	public String setName(String Name) {
		return (this.name=Name);
	}
	
	public String toString() {
		return String.format("%s\t%f",this.name, this.grade);
	}

	/*public String println() {
		return String.format("%s\t%f",this.name, this.grade);
		
	}*/

}


