package you_Tube_16_ObjectSerialization;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class ObjectSerialization {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {

		File file = new File(
				"C:\\training\\Workspacce\\Farzana\\LearnProgrammingInJava\\src\\you_Tube_16_ObjectSerialization\\students.txt");
		ArrayList<Student> students = new ArrayList<Student>();
		students.add(new Student("Tom", 3.921));
		students.add(new Student("Dave", 3.921));
		students.add(new Student("Bill", 3.921));

		// Write the entire collection to a file
		// serialize the collection of students

		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream output = new ObjectOutputStream(fos);
		for (Student s : students) {
			output.writeObject(s);
		}

		// cleanup when use file io in java.
		// close all references to the actual file itself
		output.close();
		fos.close();

		// read or deseralize the file back into collection of students

		FileInputStream fis = new FileInputStream(file);
		ObjectInputStream input = new ObjectInputStream(fis);
		ArrayList<Student> students2 = new ArrayList<Student>();
		try {
		while (true) {
			Student s = (Student) input.readObject();
			students2.add(s);
		}
		} catch (EOFException ex) {
			
		}
		for (Student s : students2) {
			System.out.println(s);
		}
		

	}

}
