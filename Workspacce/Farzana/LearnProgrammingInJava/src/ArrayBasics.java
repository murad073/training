
public class ArrayBasics {

	public static void main(String[] args) {
		// declaring array
		int[] grades = new int[4];
		// assign values
		grades[0] = 77;
		grades[1] = 85;
		grades[2] = 90;
		grades[3] = 60;
		
		// declaring arrays and initializing values at the same time
		
		String[] students = new String[] 
				{"Farzana", "Azad", "Farhana", "Fahim"};
		
		// declare variable to calculate average
		double sum = 0.0;
		
		// to print a table
			//first one is for table header
		System.out.printf("#\tstudents\tgrades\n");
			// second one is print the ----- line under the header
		System.out.printf("-\t---------\t-------\n");
		
		//start for loop to show grades and students names together
		// and to calculate average
		
		for (int i=0; i<grades.length; i++) {
			System.out.println(i + "         " + students[i]+ "           " + grades[i]);
			//System.out.printf("%d\t%s\t%d\n", i, grades[i], students[i]);
			// adding all grades to calculate average
			sum += grades[i];
		}
		
		double average = sum / grades.length;
		System.out.printf("class average %f\n", average);
		
		
		//// array of objects
		
		
		
	}

}
