package you_Tube_16_Interfaces;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

public class CashRegisterJFrame extends javax.swing.JFrame {

	private ICashRegisterDrawer drawer;
	private Object jTextFieldStatus;
	private JTextField textField;
	
	public CashRegisterJFrame() {
		
		JButton JbuttonOpenDrawer = new JButton("Open Drawer");
		JbuttonOpenDrawer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				drawer.Open();
				updateDrawerStatus();
			}
		});
		getContentPane().add(JbuttonOpenDrawer, BorderLayout.WEST);
		
		JButton JbuttonCloseDrawer = new JButton("Close drawer");
		JbuttonCloseDrawer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				drawer.Close();
				updateDrawerStatus();
			}
		});
		getContentPane().add(JbuttonCloseDrawer, BorderLayout.EAST);
		
		textField = new JTextField();
		getContentPane().add(textField, BorderLayout.SOUTH);
		textField.setColumns(10);
		
		JLabel JlblDrawerStatus = new JLabel("Drawer Status");
		getContentPane().add(JlblDrawerStatus, BorderLayout.CENTER);
		initComponents();
		
		drawer = (ICashRegisterDrawer) new DemoCashRegisterDrawer();
		updateDrawerStatus();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		
	}

	private void updateDrawerStatus() {
		((JTextComponent) this.jTextFieldStatus).setText(drawer.getDrawerStatus().toString());
		
		

		
	}
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CashRegisterJFrame frame = new CashRegisterJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

			}
	 
	//private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CashRegisterJFrame frame = new CashRegisterJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/*public CashRegisterJFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JButton JButtonOpen = new JButton("Open Drawer");
		JButtonOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		contentPane.add(JButtonOpen, BorderLayout.WEST);
		
		JButton JButtonClose = new JButton("Close Drawer");
		contentPane.add(JButtonClose, BorderLayout.EAST);
		
		JLabel JlblDrawerStatus = new JLabel("Drawer Status");
		JlblDrawerStatus.setVerticalAlignment(SwingConstants.TOP);
		contentPane.add(JlblDrawerStatus, BorderLayout.CENTER);
		
		JTextArea textArea = new JTextArea();
		contentPane.add(textArea, BorderLayout.NORTH);*/
	


