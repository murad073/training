package you_Tube_16_Interfaces;

public class DemoCashRegisterDrawer implements ICashRegisterDrawer {

	private DrawerStatus drawerStatus;

	public DemoCashRegisterDrawer() {
		this.drawerStatus = DrawerStatus.Closed;
	}

	/*
	 * public static void main(String[] args) { // TODO Auto-generated method stub }
	 */

	@Override
	public DrawerStatus getDrawerStatus() {

		return this.drawerStatus;
	}

	@Override
	public void Open() {
		this.drawerStatus = DrawerStatus.Open;

	}

	@Override
	public void Close() {
		this.drawerStatus = DrawerStatus.Closed;

	}

	@Override
	public boolean isOpen() {
		return this.drawerStatus == DrawerStatus.Open;

	}

	@Override
	public boolean isClosed() {
		return this.drawerStatus == DrawerStatus.Closed;

	}

}
