import java.util.Scanner; // Definition of scanner contains in this package. 

public class VariablesDataTypesAssignment {

	public static void main(String[] args) {
		
		// lesson 2 ///
		
		//Scanner input = new Scanner(System.in); // asking users for their input
		/*String name = "Farzana";
		//int age = 31;
		double gpa = 3.80;
		
		System.out.print("Enter student's name: ");
		name = input.nextLine(); // accept input for above line
		System.out.print("Enter student's age: ");
		age = input.nextInt(); // accept input for above line
		System.out.print("Enter student's gpa: ");
		gpa = input.nextDouble(); // accept input for above line
		
		System.out.printf("%s is %d years old. %s has a %4.2f gpa.\n", 
				         name, age, name, gpa);
		//  in printf statement there is format specifiers 
		//like %s=string, %d=int, %f=double */
		
		/*String name, heShe = "";
		boolean isFemale;
		int age, retirement = 70, daystill = 0;
		
		
		
		System.out.print("Enter your name: ");
		name = input.nextLine(); // accept input for above line
		System.out.print("What is your age? ");
		age = input.nextInt(); // accept input for above line
		System.out.print("Are you female? (true/false) : ");
		isFemale = input.nextBoolean();// accept input for above line
		
		heShe = isFemale ? "She" : "He"; // ternary oprerators
		daystill = retirement - age;
		
		System.out.printf("%s is %d years old. %s is %d years to retirement.\n", 
		         name, age, heShe, daystill); */
	
		//// lesson 3 ////
		
		/* Syntax error = an error in which java cannot build your code
		 * Semantic error = a disconnect between  a program's intent and action
		 * Integer.parseInt() explained: Integer is a class and 
		 * parseInt() is a static method of that class*/
		
		/*String x = "1";
		String y = "2";
		System.out.println(x + y);
		
		// how to convert string to integer
		int i = Integer.parseInt(x); 
		int j = Integer.parseInt(y);
		System.out.println(i + j);
		
		String gpa = "3.90";
		double gpa2 = Double.parseDouble(gpa);
		System.out.println("string value: " + gpa + " and double value: " + (gpa2 - 1.0));*/
		
		// type casting
		
		// 20/15 ==> 1.3333333333333333
		
		/*int x = 20;
		int y = 15;
		int i = x/y; // int/int ==> int , 1
		double d1 = x/y; // int/int ==> double, 1.0
		double d2 = x / (1.0 * y); // int / (double * int) ==> double
		double d3 = x/ (double)y; //implicit casting, int / double ==> double
		double d4 = (double) x/y; //implicit casting
		System.out.println(i);
		System.out.println(d1);
		System.out.println(d2);
		System.out.println(d3);
		System.out.println(d4); */
		
		//int z = (int) 3.14; // need to do explicit casting bcoz cannot convert from double to int
		//double m = 4; // implicit casting, automatically converted from int to double
		
		
		
		
		
		
	}

}
