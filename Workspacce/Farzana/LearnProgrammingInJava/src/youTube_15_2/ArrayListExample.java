package youTube_15_2;

import java.util.ArrayList;

import ArrayOfObjects.Student;

public class ArrayListExample {

	public static void main(String[] args) {
		
		// ArrayList are dynamic collections of objects (reference type only)
		ArrayList<Student> psy101 = new ArrayList<Student>();
		
		// Adding some students
		psy101.add(new Student("Bob", 2.9));
		psy101.add(new Student("Dave", 3.9));
		psy101.add(new Student("Sally", 2.5));
		psy101.add(new Student("Alice", 4.9));

		// Access as a collection
		System.out.printf("Student\tGPA\n");
		for (Student s : psy101) {
			System.out.printf("%s\n", s);
		}
		System.out.println();
		
		/*// Access with an index, similar to an array
				System.out.printf("Student\tGPA\n");
				for (int i = 0; i < psy101.size(); i++) {
					System.out.printf("%s\n", psy101.get(i));
				}*/
		
		//++++++++++++++++++++++++++++++++++++++++++++++
		System.out.println("++++++++++++++++++++++++++");
		//printStudents(psy101);
		// remove a student from position 0
		Student tmp =psy101.remove(0);
		printStudents(psy101);
		// add back at in the end
		psy101.add(tmp);
		printStudents(psy101);
		//System.out.println(psy101);
		// can add to collection at any time
		psy101.add(new Student("Bill", 3.7));
		printStudents(psy101);
		System.out.println("++++++++++++++++++++++++++");
		
		// Swap Sally(1) and Bob(2)
		tmp = psy101.get(1);// place sally in tmp
		psy101.set(1, psy101.get(3)); // put bob's in Sallys position
		psy101.set(3, tmp); // put tmp (Sally) in Bob's position
		printStudents(psy101);
		//System.out.println(psy101);
		//++++++++++++++++++++++++++++++++++++++++++\
		System.out.println("++++++++++++++++++++++++++");
		
	}

	private static void printStudents(ArrayList<Student> al) {
		System.out.printf("Student\tGPA\n");
		System.out.printf("-----------\t--------\n");
		for (int i = 0; i < al.size(); i++) {
			//al.get(i).println();
			System.out.println(al.get(i));
		}
		/*for(Student s : al) {
			s.println();
		}*/
		System.out.println();
	}

}
