package youTube_15_2;

import java.util.ArrayList;
import java.util.Collections;

import ArrayOfObjects.Student;

public class SortingSearchingStudents {
	
public static void main(String[] args) {
		
		// ArrayList are dynamic collections of objects (reference type only)
		ArrayList<Student> psy101 = new ArrayList<Student>();
		
		// Adding some students
		psy101.add(new Student("Bob", 2.9));
		psy101.add(new Student("Dave", 3.9));
		psy101.add(new Student("Sally", 2.5));
		psy101.add(new Student("Alice", 4.9));
		
		printStudents(psy101);
		
		System.out.println("SORT BY NAME");
		Collections.sort(psy101, new StudentNameComparator());
		printStudents(psy101);
		
		System.out.println("SORT BY GPA");
		Collections.sort(psy101, new StudentGpaComparator());
		printStudents(psy101);
		
}

public static void printStudents(ArrayList<Student> students) {
	System.out.printf("Student\tGPA\n");
	System.out.printf("-----------\t--------\n");
	for(Student s : students) {
		System.out.printf("%s\n", s);
	}
	System.out.println();

	
}

}
