import java.util.Scanner;

public class Functions_Switch_IfelseLadder {

	public static void main(String[] args) {
		
		/*System.out.println(rectangleArea(5, 10));
		System.out.println(rectangleArea(9, 20));
		System.out.println(rectangleArea(50, 6));
		System.out.println(rectangleParameter(7, 4));*/
		
		/*int length, width;
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the length: ");
		length = input.nextInt();
		System.out.println("Enter the width: ");
		width = input.nextInt();
		
		int area = rectangleArea(length, width);
		int per = rectangleParameter(length, width);
		
		System.out.printf("Rectangle Area: %d and Rectangle Parameter: %d\n", area, per);
		
		sayHello("mohona");*/
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter Month (1-12): ");
		int month = input.nextInt();
		System.out.println("Enter Day of Month (1-31): ");
		int day = input.nextInt();
		System.out.printf("This is the %s of %s\n", getDayname(day), getMonthName(month));
		
	}
	
	public static int rectangleArea (int length, int width) {
		return length * width;
		
	}	
	public static int rectangleParameter (int length, int width) {
			return 2* (length + width);	
	}
	
	public static void sayHello (String name) {
		System.out.printf("Hello, %s\n", name);
	}
		
    public static String getMonthName (int month) {
    
	      String result ="Unknown";
	      
	      switch (month) {
	    	  case 1:
	    		  result = "January";
	    		 break;
	    	  case 2:
	    		  result = "February";
	    		 break;
	    	  case 3:
	    		  result = "March";
	    		  break;
	    	  case 4:
	    		  result = "April";
	    		  break;
	    	  case 5:
	    		  result = "May";
	    		  break;
	    	  case 6:
	    		  result = "June";
	    		  break;
	    	  case 7:
	    		  result = "July";
	    		  break;
	    	  case 8:
	    		  result = "August";
  		      break; 
	    	  case 9:
	    		  result = "Sepetember";
  		      break; 
	    	  case 10:
	    		  result = "October";
  		      break; 
	    	  case 11:
	    		  result = "November";
  		      break; 
	    	  case 12:
	    		  result = "December";
  		      break;     	  
	      }
	      return result;
    }
	      
	
	public static String getDayname (int day) {
		String result;
		if (day !=11 && day %10==1) {
		    result = day + "st";
		}
		else if (day !=12 && day %10==2) {
			result = day + "nd";	
		}
		else if (day !=13 && day %10==3) {
			result = day + "rd";
		}
		else {
			result = day + "th";
		}
		return result;
		
		
	}

}
