package you_Tube_16_File_Input_Output;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class BasicFileIO {

	public static void main(String[] args) {
		File file = new File(
				"C:\\training\\Workspacce\\Farzana\\LearnProgrammingInJava\\src\\you_Tube_16_File_Input_Output\\test.txt");

		// Write your name and age to the file (test.txt)
		try {
			PrintWriter output = new PrintWriter(file);
			output.println("Farzana_Mohona" );
			output.println(30);

			output.close();
		} catch (IOException e) {

			System.out.printf("ERROR: %s\n", e);
		}

		System.out.println("++++++++++++ Writing Successful +++++++++++++");

		// Read from the file (test.txt)
		try {
			Scanner input = new Scanner(file);
			
			String name = input.nextLine();
			int age = input.nextInt();
			
			System.out.printf("Name: %s, Age: %d\n", name, age);
			
		} catch (FileNotFoundException ff) {
			System.out.printf("ERROR: %s\n", ff);
		} catch (InputMismatchException im) {
			System.out.printf("ERROR: %s\n", im + " >>> Please put the right input");
		}
		System.out.println("++++++++++++ Reading Successful +++++++++++++");
	}

}
