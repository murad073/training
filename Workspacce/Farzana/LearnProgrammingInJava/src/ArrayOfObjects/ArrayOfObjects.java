package ArrayOfObjects;

import java.util.Scanner;

public class ArrayOfObjects {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		String name;
		int grade;
		// creating new instance of students array
		Student[] std = new Student[4];
		
		for (int i=0; i<std.length; i++) {
		System.out.printf("Enter student grade then name for student #%d",i);
		grade = input.nextInt();
		name = input.nextLine();
		std[i] = new Student(name, grade);
		}
		
		System.out.printf("#\tstudents\tgrades\n");
		System.out.printf("-\t---------\t-------\n");
		
		// assigning value for each index
		/*new Student("farzana", 90),
		new Student("azad", 95),
		new Student("farhana", 98),
		new Student("fahim", 99)};*/
		
		for (int i=0; i<std.length; i++) {
		System.out.println(i+ std[i].getName() + std[i].getGrade());
		//System.out.printf("%d\t%s\t%d\n", i, std[i].Name(), std[i].Grade() );
		}
	}

}
