import java.util.Random;
import java.util.Scanner;

public class For_While_Loop {

	public static void main(String[] args) {
		
		/* int max=10;
		int i;
		System.out.println("Counting up.....");

		for (i=0; i<=max; i++) {
			System.out.printf("%d ", i);
		}
		//System.out.println();*/
		/*int juice;
		
		for (juice=99; juice>0; juice--) {
			System.out.printf("%d bottles of juice on the wall, ", juice);
			System.out.printf("%d bottles of juice. if one of those bottles should happen to fall, ", juice);
			System.out.printf("%d bottles of juice on the wall! \n", juice-1);

		} */
		
		/*int maxMultiple = 10;
		
		Scanner input = new Scanner (System.in);
		System.out.print("Enter the number you want multiples of : ");
		int multiplesof = input.nextInt();
		for (int i =1; i<=maxMultiple; i++) {
			int answer = i * multiplesof;
			System.out.printf("%d * %d == %d\n", i ,multiplesof, answer );
		}*/
		
		/*int max =9;
		for (int i=0; i<= max; i++) {
			for (int j=0; j<= max; j++)
			System.out.printf("[%d. %d]", i, j);
			System.out.println();

		}*/
		//System.out.println();
		
		
		//lesson --- 11: while loop
		
		// int max = 10;
		/* int i = 1;
		 System.out.println("Counting up");
		 while (i<=max) {
			 System.out.printf("%d ", i); 
			 i++;
			// System.out.println();
		 }
		System.out.println();
		
		 i = max;
		 System.out.println("Counting down");
		 while (i>=1) {
			 System.out.printf("%d ", i); 
			 i--;
			 //System.out.println();
		 } */
		 
		/* Scanner input = new Scanner (System.in);
		 Random generator = new Random();
		 int number = generator.nextInt(10) +1;
		 int guess = 0;
		 int attempt = 1;
		 System.out.println("I m thinking of a number between 1 and 10. what is it ?1?!?");
		 
		 while (guess != number) {
			 System.out.printf("Attempt %d : Your guess ==> ", attempt);
			 guess = input.nextInt();
			 attempt++;
		 }
		 System.out.printf("Finally! You guessed it. It was %d", guess); */
		
		/*Scanner input = new Scanner (System.in);
		 Random generator = new Random();
		 int number = generator.nextInt(10) +1;
		 int guess = 0;
		 int attempt = 1;
		 System.out.println("I m thinking of a number between 1 and 10. what is it ?1?!?");
		 
		 do {
			 System.out.printf("Attempt %d : Your guess ==> ", attempt);
			 guess = input.nextInt();
			 attempt++;
		 }
		 while (guess != number);
		 System.out.printf("Finally! You guessed it. It was %d", guess); */
		 
		 Scanner input = new Scanner (System.in);
		 Random generator = new Random();
		 int number = generator.nextInt(10) +1;
		 int guess = 0;
		 int attempt = 1;
		 System.out.println("I m thinking of a number between 1 and 10. what is it ?1?!?");
		 
		 while (true) {
			 System.out.printf("Attempt %d : Your guess ==> ", attempt);
			 guess = input.nextInt();
			 attempt++;
			 if (guess == number) {
				 System.out.printf("Finally! You guessed it. It was %d", guess);
				 break;
			 } else if (guess > 10 || guess < 1) {
			 System.out.printf("Common! thats not between 1 and 10 you goof! \n");

		 } else {
			 System.out.printf("Nope! thats not it!!\n");

		 }
	}
				


	}

}
