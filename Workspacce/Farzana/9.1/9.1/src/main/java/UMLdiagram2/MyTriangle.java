package UMLdiagram2;

public class MyTriangle {
	
	//Three private instance variables v1, v2, v3 (instances of MyPoint), for the three vertices.
	private MyPoint v1;
	private MyPoint v2;
	private MyPoint v3;
	//A constructor that constructs a MyTriangle with three set of coordinates, v1=(x1, y1), v2=(x2, y2), v3=(x3, y3).
	public MyTriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
		this.v1 = new MyPoint(x1, y1);
		this.v2 = new MyPoint(x2, y2);
		this.v3 = new MyPoint(x3, y3);
	}
	//An overloaded constructor that constructs a MyTriangle given three instances of MyPoint.
	public MyTriangle(MyPoint v1, MyPoint v2, MyPoint v3) {
		this.v1 = v1;
		this.v2 = v2;
		this.v3 = v3;

	}
	//A toString() method that returns a string description of the instance in the format "MyTriangle[v1=(x1,y1),v2=(x2,y2),v3=(x3,y3)]".
	public String toString() {

		// return "MyTriangle[v1=(x1,y1), v2=(x2,y2), v3=(x3,y3)]";
		return "MyTriangle[v1=(" + v1 + "), v2=(" + v2 + "), v3=(" + v3 + ")]";
	}
	//A getPerimeter() method that returns the length of the perimeter in double. You should use the distance() method of MyPoint to compute the perimeter.
	public double getPerimeter() {
		/*
		 * double sideA; double sideB; double sideC; double length; if (length=
		 * v1.distance(v2)) { sideA = length; } if (length= v2.distance(v3)) { sideB =
		 * length; } if (length= v1.distance(v3)) { sideC = length; }
		 * 
		 * return sideA + sideB + sideC;
		 */

		return v1.distance(v2) + v2.distance(v3) + v1.distance(v3);

	}
	//A method printType(), which prints "equilateral" if all the three sides are equal, "isosceles" if any two of the three sides are equal, or "scalene" if the three sides are different.
	public String getType() {
		double d1 = v1.distance(v2);
		double d2 = v2.distance(v3);
		double d3 = v1.distance(v3);

		if (d1 == d2 && d2 == d3) {
			return "This Triangle is equilateral; Means all its sides of the same length";
		} else if (d1 == d2 || d1 == d3 || d2 == d3) {
			return "This Triangle is isosceles; Means two sides of equal length";
		}
		return "This Triangle is scalene; Means unequal in length";
	}

}
