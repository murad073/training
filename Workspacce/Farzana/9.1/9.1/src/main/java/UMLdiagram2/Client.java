package UMLdiagram2;

public class Client {
	
	//Create a client class (with main method) and show the use of all the public methods.

	public static void main(String[] args) {
		MyTriangle mt = new MyTriangle (5, 6, 7, 8, 9, 10);
		System.out.println(mt.toString());
		System.out.println(mt.getPerimeter());
		System.out.println(mt.getType());

	}

}
