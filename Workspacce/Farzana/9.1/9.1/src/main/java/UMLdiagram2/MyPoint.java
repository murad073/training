package UMLdiagram2;

public class MyPoint {
	private int x;
	private int y;

	public MyPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public double distance(int x, int y) {
		int xDiff = this.x - x;
		int yDiff = this.y - y;
		return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
	}

	public double distance(MyPoint p) {
		int xDiff = this.x - p.getX();
		int yDiff = this.y - p.getY();
		return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
	}

	public String toString() {
		return "(" + this.x + ", " + this.y + ")";
	}

}
