package Exercise.Class20;

import java.util.Random;

public class U2 extends Rocket{
	public U2() {
		super.maxWeightLimit = 29 * 1000; // to convert into KG from tones
		super.currentWeight = 18 * 1000; // to convert into KG from tones
	}
	@Override
	public boolean launch() {
		double probability = (0.04 * super.currentWeight / super.maxWeightLimit);// 5% // range 0 to 5
		double randomNumber = new Random().nextDouble() * 100 + 1; // range 1 to 100
		return probability < randomNumber;
	}
	
	@Override
	public boolean land() {
		double probability = (0.08 * super.currentWeight / super.maxWeightLimit);// 5% // range 0 to 5
		double randomNumber = new Random().nextDouble() * 100 + 1; // range 1 to 100
		return probability < randomNumber;
	}



}
