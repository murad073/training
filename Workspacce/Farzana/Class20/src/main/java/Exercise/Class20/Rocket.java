package Exercise.Class20;

public class Rocket implements Spaceship{
	
	public int currentWeight;
	public int maxWeightLimit;

	public boolean launch() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean land() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean canCarry(Item item) {
		int approxWeight = this.currentWeight + item.weight;
		if (approxWeight <= this.maxWeightLimit) {
			return true;
		}
		return false;
	}

	public void carry(Item item) {
		this.currentWeight += item.weight;
		
	}

}
