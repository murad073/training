package Exercise.Class20;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Simulation {
	
	public ArrayList<Item> loadItems() throws IOException{
		
		ClassLoader loader = getClass().getClassLoader();
		File file = new File(loader.getResource("phase-1.txt").getFile());
		List<String> allLines = Files.readAllLines(file.toPath());
		
		ArrayList<Item> allItems = new ArrayList<Item>();
		
		for(int i=0; i<allLines.size(); i++) {
			String line = allLines.get(i);
			String[] parts = line.split("=");
			
			Item item1 = new Item();
			item1.name = parts[0];
			item1.weight = Integer.parseInt(parts[1]);
			
			allItems.add(item1);
		}
		
//		ClassLoader loader2 = getClass().getClassLoader();
//		File file2 = new File(loader.getResource("phase-2.txt").getFile());
//		List<String> allLines2 = Files.readAllLines(file.toPath());
//		
//		
//		for(int i=0; i<allLines2.size(); i++) {
//			String line = allLines2.get(i);
//			String[] parts2 = line.split("=");
//			Item item2 = new Item();
//			item2.name = parts2[0];
//			item2.weight = Integer.parseInt(parts2[1]);
//			
//		}
		return allItems;	
	}
	
	
	
	
	
}
