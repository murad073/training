package Exercise.Class20;

import java.util.Random;

public class U1 extends Rocket{
	
	public U1() {
		super.maxWeightLimit = 18 * 1000; // to convert into KG from tones
		super.currentWeight = 10 * 1000; // to convert into KG from tones
	}
	@Override
	public boolean launch() {
		double probability = (0.05 * super.currentWeight / super.maxWeightLimit);// 5% // range 0 to 5
		double randomNumber = new Random().nextDouble() * 100 + 1; // range 1 to 100
		return probability < randomNumber;
	}
	
	@Override
	public boolean land() {
		double probability = (0.01 * super.currentWeight / super.maxWeightLimit);// 5% // range 0 to 5
		double randomNumber = new Random().nextDouble() * 100 + 1; // range 1 to 100
		return probability < randomNumber;
	}


}
