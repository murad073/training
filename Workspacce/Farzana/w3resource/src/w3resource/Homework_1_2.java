package w3resource;

public class Homework_1_2 {

	public static void main(String[] args) {
		/* 1. Write a Java program to print 'Hello' on screen 
					and then print your name on a separate line. */
				
				System.out.println("Exercise-1:\nHello\nFarzana Mohona!!!");
				
				// 2. Write a Java program to print the sum of two numbers. 
				
				System.out.print("Exercise-2: ");
				System.out.println(74+36);
				
				/* 3. Write a Java program to divide two numbers 
					and print on the screen. */
				System.out.print("Exercise-3: ");
				System.out.println(50/3);
				
				/* 4. Write a Java program to print the result 
					//of the following operations. */
				
				System.out.println("Exercise-4: ");
				System.out.println(-5 + 8 * 6);
				System.out.println((55+9) % 9);
				System.out.println(20 + -3*5 / 8);
				System.out.println(5 + 15 / 3 * 2 - 8 % 3);

	}

}
