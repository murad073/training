package PracticeSingleton.Class12;

import java.util.HashMap;
import java.util.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       /*MySingleton s = MySingleton.Instance;
       
       System.out.println(s.getSomething()); */
    	
    	// the Hash Map class
    	
    	HashMap m = new HashMap();
    	m.put("Farzana", 4.1);
    	m.put("Kibria", 4.5);
    	m.put("Ruzina", 4.3);
    	m.put("Farzana", 5.0);
    	
    	System.out.println(m.get("Farzana"));
    }
}
