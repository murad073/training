package PracticeSingleton.Class12;

public class MySingleton {
	
	private MySingleton() {
		 
	 }
	public static MySingleton Instance = new MySingleton();
	
	public String getSomething() {
		return "this is something!";
	}
}
 