package personExercise;

public class Student extends Person {
	// Declaring variables for Student class
	private String Program;
	private int Year;
	private double Fee;

	// Forcing constructor to put mandatory parameters
	public Student(String Name, String Address, String Program, int Year, double Fee) {
		// need to mention super to get name & address from Super or Parent class
		// (Person)
		super(Name, Address);
		// super.Name = Name;
		// super.Address = Address;
		// Indicating students variables are equal to parameters variables
		this.Program = Program;
		this.Year = Year;
		this.Fee = Fee;

		/*
		 * setProgram(this.Program); setYear(this.Year); setFee(this.Fee);
		 */
	}
	// GET & SET METHOD

	// Get & Set method for setting and getting Program
	public String getProgram() {
		return this.Program;
	}

	public void setProgram(String Program) {
		this.Program = Program;
	}

	// Get & Set method for setting and getting Year
	public int getYear() {
		return this.Year;
	}

	public void setYear(int Year) {
		this.Year = Year;
	}

	// Get & Set method for setting and getting Fee
	public double getFee() {
		return this.Fee;
	}

	public void setFee(double Fee) {
		this.Fee = Fee;
	}

	// ........................END of Method...................//

	// String method to print string as output.
	public String toString() {
		return "Student[Person[Name= " + super.Name + ", Address= " + super.Address + "], Program= " + this.Program
				+ ", Year= " + this.Year + ", Fee= " + this.Fee + "]";

	}
}
