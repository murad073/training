package personExercise;

public class Staff extends Person {
	// Declaring variables for Staff class
	private String School;
	private double Pay;

	// Forcing constructor to put mandatory parameters
	public Staff(String Name, String Address, String School, double Pay) {
		// need to mention super to get name & address from Super or Parent
		// class(Person)
		super(Name, Address);
		// Indicating staffs variables are equal to parameters variables
		this.School = School;
		this.Pay = Pay;
	}
	// GET & SET METHOD

	// Get & Set method for setting and getting school name

	public String getSchool() {
		return this.School;
	}

	public void setSchool(String School) {
		this.School = School;
	}

	// Get & Set method for setting and getting pay
	public double getPay() {
		return this.Pay;
	}

	public void setPay(double Pay) {
		this.Pay = Pay;
	}

	// ........................END of Method...................//

	// String method to print string as output.
	public String toString() {
		return "Staff[Person[Name= " + super.Name + ", Address= " + super.Address + "], School= " + this.School
				+ ", Pay= " + this.Pay + "]";

	}
}
