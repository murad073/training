package personExercise;

public class Person {
	// Declaring variables for person class
	protected String Name;
	protected String Address;

	// Forcing constructor to put mandatory parameters
	public Person(String Name, String Address) {
		// Indicating person variable name equal parameter name
		this.Name = Name;
		this.Address = Address;
	}

	// GET & SET METHOD

	// Get & Set method for setting and getting Name

	public String getName() {
		return this.Name;
	}

	// public void setName(String Name) { this.Name=Name; }

	// Get & Set method for setting and getting Address
	public String getAddress() {
		return this.Address;
	}

	public void setAddress(String Address) {
		this.Address = Address;
	}

	// ........................END of Method...................//

	// String method to print string as output.
	public String toString() {
		return "Person[Name= " + this.Name + ", Address= " + this.Address + "]";
	}
	
	
}
