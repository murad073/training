package clientExercise;

import personExercise.Person;
import personExercise.Staff;
import personExercise.Student;

public class Client {

	public static void main(String[] args) {

		Person person1 = new Person("Moshiur", "Sterling");
		System.out.println(person1);
		System.out.println("------------------------------------------------------------------------------");

		Student student1 = new Student("Farzana", "Sterling", "Masters", 2016, 2000);
		System.out.println(student1.toString());
		// System.out.println(student1.getYear());
		System.out.println("Student name is " + student1.getName() + " and program is " + student1.getProgram());
		System.out.println("------------------------------------------------------------------------------");

		Staff staff1 = new Staff("Zakir", "Falls Church", "VIU", 4000);
		System.out.println(staff1.toString());
		System.out.println("Staff name is " + staff1.getName() + " and works at " + staff1.getSchool());
		
		//https://www.javatpoint.com/understanding-toString()-method

	}

}
