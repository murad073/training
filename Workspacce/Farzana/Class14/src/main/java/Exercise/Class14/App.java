package Exercise.Class14;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;

/**
 * This class is used for file read purpose.
 *@deprecated
 */
public class App {
	
	/**
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
	
	//----------------- Using builtin java io----------------	
		// throws FileNotFoundException
		// File file1 = new File("C:\\Users\\UserDB\\Downloads\\movieList.txt");
		// Scanner scan = new Scanner(file1);
		// scan.useDelimiter("\\Z");
		// System.out.println( scan.next());
		
		//------------------ Using Apache common library-----------------

//		File file1 = new File("C:\\Users\\UserDB\\Downloads\\movieList.txt");
//		System.out.println(FileUtils.readFileToString(file1));
		
		// Using read file from resources
		
		ClassLoader loader = App.class.getClassLoader();
		File file1 = new File(loader.getResource("users.json").getFile());
		//System.out.println(FileUtils.readFileToString(file1));
		Scanner scan = new Scanner(file1);
		scan.useDelimiter("\\Z");
		System.out.println( scan.next());
	}
}
