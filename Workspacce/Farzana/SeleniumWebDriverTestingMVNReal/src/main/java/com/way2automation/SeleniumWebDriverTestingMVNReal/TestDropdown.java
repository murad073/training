package com.way2automation.SeleniumWebDriverTestingMVNReal;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TestDropdown {

	public static void main(String[] args) {

		/*
		 * System.setProperty("webdriver.gecko.driver",
		 * "C:\\Selenium_Prerequisite\\FirefoxDriver\\geckodriver-v0.22.0-win64\\geckodriver.exe"
		 * ); WebDriver driver = new FirefoxDriver();
		 */

		System.setProperty("webdriver.chrome.driver",
				"C:\\Selenium_Prerequisite\\chromedriver\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.wikipedia.org/");
		// driver.findElement(By.xpath("//select[@id='searchLanguage']")).sendKeys("English");

		Select select = new Select(driver.findElement(By.xpath("//select[@id='searchLanguage']")));
		// select.selectByVisibleText("English");
		select.selectByValue("hi");

		System.out.println(" ");
		System.out.println("@@@@@@@@@@ Printing all dropdown values @@@@@@@@@@");
		System.out.println(" ");
		
		WebElement dropdown = driver.findElement(By.id("searchLanguage"));

		// List<WebElement> options = select.getOptions(); // or
		// List<WebElement> options = driver.findElements(By.tagName("option"));

		List<WebElement> options = dropdown.findElements(By.tagName("option"));

		System.out.println("Printing total options from dropdown list: "+options.size());

		System.out.println("---------------------Options Text (Country Name)----------------");
		for (int i = 0; i <= options.size() - 1; i++) {

			System.out.println(options.get(i).getText());
		}

		System.out.println("-----------Options Attribute (Language Support)--------");
		for (int i = 0; i <= options.size() - 1; i++) {

			// System.out.println(options.get(i).getText());

			System.out.println(options.get(i).getAttribute("lang"));
		}

		System.out.println(" ");
		System.out.println("@@@@@@@@@@ Printing all links @@@@@@@@@@");
		System.out.println(" ");
		

		List<WebElement> links = driver.findElements(By.tagName("a"));

		for (int i = 0; i <= links.size() - 1; i++) {

			System.out.println(links.get(i).getAttribute("href"));
			// System.out.println(links.get(i).getText());
		}
		
		System.out.println(" ");
		System.out.println("Printinking all links size-------------------------: " + links.size());
		System.out.println(" ");

		System.out.println("@@@@@@@@@@ Printing specific block links @@@@@@@@@@");
		System.out.println(" ");

		WebElement block = driver.findElement(By.cssSelector("div.footer")); // xpath=//body[@id='www-wikipedia-org']/div[6]

		List<WebElement> links2 = block.findElements(By.tagName("a"));

		for (int i = 0; i <= links2.size() - 1; i++) {

			// System.out.println(links2.get(i).getAttribute("href"));
			System.out.println(links2.get(i).getText());
		}
		
		System.out.println(" ");
		System.out.println("Printinking specific block size-------------------------: " + links2.size());
		System.out.println(" ");
		
		System.out.println("______________Success________________");

	}
}