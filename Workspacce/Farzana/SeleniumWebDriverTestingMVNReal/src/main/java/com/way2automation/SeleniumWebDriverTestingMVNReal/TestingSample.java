package com.way2automation.SeleniumWebDriverTestingMVNReal;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Hello world!
 *
 */
public class TestingSample {
	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		/*
		 * System.setProperty("webdriver.gecko.driver",
		 * "C:\\Selenium_Prerequisite\\FirefoxDriver\\geckodriver-v0.22.0-win64\\geckodriver.exe"
		 * ); WebDriver driver = new FirefoxDriver();
		 */

		System.setProperty("webdriver.chrome.driver",
				"C:\\Selenium_Prerequisite\\chromedriver\\chromedriver_win32\\chromedriver.exe");
		// ChromeDriver driver = new ChromeDriver();
		WebDriver driver = new ChromeDriver();
		driver.get("https://gmail.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		//WebDriverWait wait = new WebDriverWait(driver, 5); // here 5 is second
		System.out.println(driver.getTitle());
		
		
		// Waiting 30 seconds for an element to be present on the page, checking
		   // for its presence once every 5 seconds.
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
			       .withTimeout(30, TimeUnit.SECONDS)
			       .pollingEvery(5, TimeUnit.SECONDS)
			       .withMessage("Timeout after 30 seconds user defined")
			       .ignoring(NoSuchElementException.class);

		// WebElement username = driver.findElement(By.id("identifierId"));
		// username.sendKeys("mohona1987@gmail.com");
		// username.findElement(By.xpath("//span[@class='RveJvd snByac']")).click();
		/*
		 * WebElement password =
		 * driver.findElement(By.xpath("//span[@class='RveJvd snByac']"));
		 * //className("RveJvd snByac")); password.sendKeys("mkhnn"); password.click();
		 */

		driver.findElement(By.id("identifierId")).sendKeys("mohona1987@gmai.com");
		driver.findElement(By.xpath(".//*[@id='identifierNext']/content/span ")).click();// xpath=//div[@id='identifierNext']/div[2]
																							// ,
																							// //div[@id='identifierNext
		 //Thread.sleep(1000); // 1000 milisecond = 1 second
		System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				".//*[@id='view_container']/div/div/div[2]/div/div[1]/div/form/content/section/div/content/div[1]/div/div[2]/div[2]/div"))).getText());
		/*System.out.println(driver.findElement(By.xpath(
				".//*[@id='view_container']/div/div/div[2]/div/div[1]/div/form/content/section/div/content/div[1]/div/div[2]/div[2]/div"))
				.getText());*/
		/*Thread.sleep(1000);
		driver.findElement(By.name("password")).sendKeys("kkkkk");
		driver.findElement(By.xpath(".//*[@id='passwordNext']/content/span")).click(); // div[@id='passwordNext']/content
		Thread.sleep(1000);
		System.out.println(driver.findElement(By.xpath("//*[@id=\"password\"]/div[2]/div[2]")).getText()); // div[@id='password']/div[2]/div[2]
		driver.close(); // close the current page
		driver.quit(); // kill the session + close all related pages*/
	}
	// //*[@id="password"]/div[2]/div[2] //*[@id="password"]/div[2]/div[2]
	//
}
