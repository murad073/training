package ShapeExercise;

public class Triangle extends Shape {
	private int base;
	private int height;
	private int side1;
	private int side2;
	private int side3;
	
	public Triangle(int base, int height) {
		this.base= base;
		this.height= height;
	}
	public Triangle(int side1, int side2, int side3) {
		this.side1= side1;
		this.side2= side2;
		this.side3= side3;
	}

	@Override
	public double getArea() {
		int area = (base*height)/2;
		return area;
	}

	@Override
	public double getPerimeter() {
		int perimeter = side1 + side2 + side3;
		return perimeter;
	}

}
