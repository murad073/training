package ShapeExercise;

public class Circle  extends Shape{
	
	private int radius;
	
	public Circle(int radius) {
		this.radius =radius;
	}

	@Override
	public double getArea() {
		
		double area = 3.1416 * this.radius * this.radius;
		return area;
	}

	@Override
	public double getPerimeter() {
		
		double perimeter = 2 * 3.1416 * this.radius;
		
		return perimeter;
	}
	
}
