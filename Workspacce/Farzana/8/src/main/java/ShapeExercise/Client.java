package ShapeExercise;

import java.util.*;
//import java.util.ArrayList;
//import java.util.List;

public class Client {

	public static void main(String[] args) {

		Client c = new Client();
		List list = new ArrayList();

		/*list.add(c.createShape("circle"));
		list.add(c.createShape("rectangle"));
		list.add(c.createShape("square"));*/

		for (int i = 0; i < list.size(); i++) {
			System.out.println(((Shape) list.get(i)).getArea());

		}

		// Circle c = new Circle (8);
		// c.getArea();

		// Client c = new Client();
		/*
		 * Shape shape1 = c.createShape("circle");
		 * 
		 * System.out.println(shape1.getPerimeter()); Shape shape2 =
		 * c.createShape("rectangle"); Shape shape3 = c.createShape("square");
		 */

		/*Shape shape1 = c.createShape("circle");
		Shape shape2 = c.createShape("square");
		//Shape shape3 = c.createShape("rectangle"); */
		//Shape shape4 = c.createShape("triangle");
		
		Shape shape1 = c.createShape(ShapeType.Circle);
		Shape shape2 = c.createShape(ShapeType.Triangle);
		Shape shape3 = c.createShape(ShapeType.Rectangle);
		
		System.out.println("Circle's result: " + shape1.getPerimeter());
		System.out.println("Square's result: " + shape2.getPerimeter());
		//System.out.println("Rectangle's result: " + shape3.getPerimeter());
		//System.out.println("Triangle's result: " + shape4.getPerimeter());
		//System.out.println("Triangle's result for area: " + shape4.getArea());


		/*
		 * System.out.println(shape1.getArea()); System.out.println(shape2.getArea());
		 * System.out.println(shape3.getArea());
		 */

		// method who can create any type of object

	}

	/*public Shape createShape(String type) {

		Shape result = null;

		if (type == "circle") {
			result = new Circle(5);
		} else if (type == "rectangle") {
			result = new Rectangle(4, 6);
		} else if (type == "square") {
			result = new Square(6);
		} else if (type == "triangle") {
			result = new Triangle(18,6);
		} //else if (type == "triangle1") {
			//result = new Triangle(2, 5, 3);
		//}
		return result;
	}*/
	public Shape createShape(ShapeType type) {

		Shape result = null;

		if (type == ShapeType.Circle) {
			result = new Circle(5);
		} else if (type == ShapeType.Rectangle) {
			result = new Rectangle(4, 6);
		} else if (type == ShapeType.Square) {
			result = new Square(6);
		} else if (type == ShapeType.Triangle) {
			result = new Triangle(18,6);
		} //else if (type == "triangle1") {
			//result = new Triangle(2, 5, 3);
		//}
		return result;
	}
	
	

}
