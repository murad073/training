package Class_16.Exception_Basic;

import java.util.InputMismatchException;
import java.util.Scanner;

public class customExceptions {

	public static void main(String[] args) {

		int i = 0;
		boolean done = false;

		do {
			try {
				System.out.print("Enter an Integer between 1 and 10: ");
				Scanner input = new Scanner(System.in);
				i = input.nextInt();
				if (i < 1 || i > 10)
					throw new IntegerOutOfRangeException();

				System.out.printf("Good Job!... You entered Integer %d\n", i);

			} catch (IntegerOutOfRangeException e) {
				// System.out.println(e);
				System.out.println(e + " :- Your input value is not in the specified range");
			} catch (InputMismatchException ex) {
				// System.out.println(ex);
				System.out.println(ex + " :- You did not enter an Integer");
			} finally {
				System.out.println("This happens whether the exception happens or not");

			}
		} while (!done);

	}

}
