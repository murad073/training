package Exercise_13;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		
			try {
			int result = divide(5, 0);
			System.out.println(result);
		} catch (Exception ex) {
			System.out.println("Not a valid operation - " + ex);
			try {
				divide(4, 0);
			} catch (Exception err) {
				System.out.println("Error again!! " + err);
			}
			/*
			 * catch () { finally { }
			 */
		}
			finally {
				System.out.println("I am in finally");
			}
			
		}

	

	public static int divide(int num1, int num2) throws NotAllowedZeroAsSecondParameter {
		if (num2 == 0) {
			throw new NotAllowedZeroAsSecondParameter();
		}
		return num1 / num2;
	}
}
