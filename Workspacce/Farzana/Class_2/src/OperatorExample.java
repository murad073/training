
public class OperatorExample {

	public static void main(String[] args) {
		// int x=10;  
		//System.out.println(x++);//10 (11)  
		//System.out.println(++x);//12  
		//System.out.println(x--);//12 (11)  
		//System.out.println(--x);//10  
        
		// x++;
		 //System.out.println("x value:" + x); // result-11
		 //int y = x++; // putting ++ sign after means: it increased by 1 after executing the line.
		 // result - x= 11, y=10
		//int y = ++x; // here, 1 will increase before executing the line
		// result x= 11, y=11
		// int y = (++x) + (x++); 
		 // result: y= (++x= (10+1)=11 and x++ = 11. so y= 22. but x= 12. 
		 // because increased by 1 after executing the line.
		 //System.out.println("x value: " + x + " " + "y value: " + y); // result x= 11 and y =10
		//System.out.println(true);
		//System.out.println(!false);
		//System.out.println(!false && !true);
		//System.out.println(false || true);
		//System.out.println((false || true) && (false || true));
		//System.out.println((false || true) || (false || true));
		//System.out.println((false || true) && (false && true));
		//System.out.println((false && true) && true);
		//System.out.println((false && false) || true);
		//System.out.println((false || false) || (false || false));
		//System.out.println((false || false) || (false || true));
		//System.out.println(1 > 2 || 2 > 1);
		//System.out.println(1 > 1 || 2 > 2);
		//System.out.println(1 >= 1 && 2 >= 2);
		//System.out.println(1 >= 1 && 2 == 2 );
		//int x = 10;
		//System.out.println(++x == x++ ); // 11 == 11, true
		//System.out.println(x == x++); // 10 == 10, true
		//System.out.println(x++ == ++x ); // 10 == 11, false
		//System.out.println( x++ > ++x); // 10 > 11, false
		//System.out.println(++x >= x++); // 11 >= 11, true
		//int x = 1, y = 2;
		//System.out.println( (x>y) && (y>x));
		//System.out.println((x<y) && (y>x));
		//System.out.println((x>y) || (y<x));
		//System.out.println((x<y) && !(y>x));
		//System.out.println((x==y) && !(y==x));
		//System.out.println((x!=y) && !(y==x));
		//System.out.println(x++ + y++); // 1 + 2 =3
		//System.out.println( ++x + y++); // 2 + 2=4
		//System.out.println(++x + ++y + x++); // 2 + 3 + 2=7
		//System.out.println(x++ + x++); // 1 + (1+1)=3
		//System.out.println(++x + ++x); // 2+ 3=5
		//System.out.println(x++ + ++x); // 1 + (1+1)+1=4
		//System.out.println(++x + x++);// 2+ 2 =4
		//System.out.println(x+y >= y+x); //3 >= 3, true
		//System.out.println(x+y != y+x); // 3 != 3, false
		//Integer x =2, y = -3;
		//System.out.println(x > y ); // true
		//System.out.println(x == y); // false
		//System.out.println(  x*3 == y*2 ); //false
		//System.out.println( x*x > y*y); // 4 > 9, false
		//System.out.println( x*y <= y*y); // -6 <= 9, true
		//System.out.println( (x*y <= y*y) && (true)); // -6 <= 9 && true, true
		//System.out.println( (x*y <= y*y) && !(true)); // -6 <= 9 && !true, false
		
		//int a =  20;
		//int b = 20;
		//System.out.println("a is " + (a++ + ++a) + " and b is " + (b++ + b++));
		
		int x = 15;   int y = 4;
		int result = x / y;
		System.out.println(result);


	}

}
