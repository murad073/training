/**
 * 
 */
package singleton_Design_Pattern;

/**
 * @author UserDB
 *
 */
public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//illegal construct
	      //Compile Time Error: The constructor SingleObject() is not visible
	      //SingleObject object = new SingleObject();

	      //Get the only object available
		PracticeSingleton object = PracticeSingleton.getInstance();

	      //show the message
	      object.showMessage();

	}

}
