/**
 * 
 */
package singleton_Design_Pattern;



/**
 * @author UserDB
 *
 */
public class PracticeSingleton {
	
	// Private constructor: It will prevent to instantiate the Singleton class from outside the class.
	//make the constructor private so that this class cannot be
	   //instantiated
	private PracticeSingleton() {
		 
	 }
	//Static member: It gets memory only once because of static, itcontains the instance of the Singleton class.
	//static member holds only one instance of the PracticeSingleton class. 
	//create an object of SingleObject
	public static PracticeSingleton Instance = new PracticeSingleton(); //Early, instance will be created at load time 
	
	
	//Get the only object available
	   public static PracticeSingleton getInstance(){
	      return Instance;
	   }

	   public void showMessage(){
	      System.out.println("Hello World!");
	   }
	
	
	//Static factory method: This provides the global point of access to the Singleton object and returns the instance to the caller.
	//  lazy Instantiation
	
	/* public static PracticeSingleton getPracticeSingleton(){  
   if (Instance == null){  
      synchronized(Singleton.class){  
        if (Instance == null){  
            Instance = new Singleton();//instance will be created at request time  
        }  
    }              
    }  
  return Instance;   
 }  */
	

}
