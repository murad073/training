/**
 * 
 */
package strategy_Design_Pattern;

/**
 * @author UserDB
 *
 */
public class Context {
	private Strategy strategy;

	   public Context(Strategy strategy){
	      this.strategy = strategy;
	   }

	   public int executeStrategy(int num1, int num2){
	      return strategy.doOperation(num1, num2);
	   }

}
