/**
 * 
 */
package strategy_Design_Pattern;

/**
 * @author UserDB
 *
 */
public interface Strategy {
	public int doOperation(int num1, int num2);;
}
