package automatedTesting;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class WebTesting {

	public static String browser = "Chrome";
	public static WebDriver driver;
	
	/*public static ChromeDriver driver;
	public static FirefoxDriver driver1;
	public static InternetExplorerDriver driver2;*/

	public static void main(String[] args) {
		
		
		/*WebDriver driver = new ChromeDriver(); 
		WebDriver driver1 = new FirefoxDriver(); 
		WebDriver driver2 = new InternetExplorerDriver();*/

		if (browser.equals("Chrome")) {
			driver = new ChromeDriver();
		} else if (browser.equals("Firefox")) {
			driver = new FirefoxDriver();
			//driver1 = new FirefoxDriver();
		} else if (browser.equals("IE")) {
			driver = new InternetExplorerDriver();
			//driver2 = new InternetExplorerDriver();
		}

		// creating an object
		/*
		 * ChromeDriver driver = new ChromeDriver(); 
		 * FirefoxDriver driver1 = new FirefoxDriver(); 
		 * InternetExplorerDriver driver2 = new mInternetExplorerDriver();
		 */

		// calling CHROME method via reference variable
		driver.Navigate();
		driver.Open();
		driver.getTitle();
		driver.Click();
		driver.Type();
		
		// calling FirefoxDriver method via reference variable
		/*driver1.Navigate();
		driver1.Open();
		driver1.getTitle();
		driver1.Click();
		driver1.Type();
		// calling InternetExplorerDriver method via reference variable
		driver2.Navigate();
		driver2.Open();
		driver2.getTitle();
		driver2.Click();
		driver2.Type();*/

	}

}
