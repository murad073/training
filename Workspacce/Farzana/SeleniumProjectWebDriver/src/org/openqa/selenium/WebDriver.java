package org.openqa.selenium;

public interface WebDriver {
	
	public void Navigate();

	public void Open();

	public void Click();

	public void getTitle();

	public void Type();

	public void SendKeys();

}
