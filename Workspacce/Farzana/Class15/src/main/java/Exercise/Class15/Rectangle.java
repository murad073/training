/**
 * 
 */
package Exercise.Class15;

/**
 * @author UserDB
 *
 */
public class Rectangle implements Drawable {

	/* (non-Javadoc)
	 * @see Exercise.Class15.Drawable#draw()
	 */
	@Override
	public void draw() {
		System.out.println("drawing rectangle"); 
	}
	
	@Override
	public void msg() {
		System.out.println("overriding the default msg function"); 
	}
}
