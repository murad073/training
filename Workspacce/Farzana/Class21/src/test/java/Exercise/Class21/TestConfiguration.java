package Exercise.Class21;

//import org.testng.annotations.AfterClass;
//import org.testng.annotations.AfterGroups;
//import org.testng.annotations.AfterMethod;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.BeforeGroups;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
import org.testng.annotations.*;

public class TestConfiguration {

	@Test
	public void runTest2() {
		System.out.println("@Test - runTest2");
	}

	@Test
	public void runTest3() {
		System.out.println("@Test - runTest3");
	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("@BeforeMethod");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("@AfterMethod");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("@BeforeClass");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("@AfterClass");
	}

	@BeforeGroups("smoke")
	public void beforeGroups() {
		System.out.println("@BeforeGroups");
	}

	@AfterGroups("smoke")
	public void afterGroups() {
		System.out.println("@AfterGroups");
	}

	@Test(groups = "smoke")
	public void runTest1() {
		System.out.println("@Test - runTest1");
	}
	
	//////////////////////////testSuite and test///////////////////////
	/*@BeforeSuite()
	public void beforeSuite() {
		System.out.println("@BeforeSuite");
	}

	@AfterSuite()
	public void afterSuite() {
		System.out.println("@AfterSuite");
	}

	@BeforeTest()
	public void beforeTest() {
		System.out.println("@BeforeTest");
	}

	@AfterTest()
	public void afterTest() {
		System.out.println("@AfterTest");
	} */

}
