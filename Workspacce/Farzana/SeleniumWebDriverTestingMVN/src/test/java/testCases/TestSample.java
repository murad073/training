package testCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver; // CTRL +SHIFT + O =to import all packages

public class TestSample {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		//driver.get("http://gmail.com");
		driver.navigate().to("http://gmail.com"); // do the same work like driver.get("http://gmail.com");
		// driver.navigate() has more functionality
		//System.out.println(driver.getTitle()); 
		// instead of printing directly can store in a variable so that can compare
		
		//driver.navigate().back();
		driver.manage().window().maximize();
		
		
		String actual_title = driver.getTitle(); // get the result from website
		//System.out.println(actual_title.length());
		
		String expected_title = "gmail"; // may come this result from excel
		if (actual_title.equalsIgnoreCase(expected_title)) {
			System.out.println("test case is passed");
		} else {
			System.out.println("test case is failed");
		}
		
		System.out.println(driver.getTitle().length());
		driver.close();// just close the current browser in which i am focusing at the moment
		driver.quit(); // quit kills the entire session not the windows which are already open
		

	}

}
