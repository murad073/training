package testCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestFindingElements {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		driver.get("https://gmail.com");
		
		/*WebElement userName = driver.findElement(By.id("identifierId"));
		userName.sendKeys("mohona1987@gmail.com");
		
		// xpath= //tagName[@attribute='value']
		WebElement nextButton = driver.findElement(By.xpath("//input[@id='identifierNext']"));
		nextButton.click();*/
		driver.findElement(By.id("identifierId")).sendKeys("mohona1987@gmail.com");
		driver.findElement(By.xpath("//input[@id='identifierNext']")).click();
	}
}