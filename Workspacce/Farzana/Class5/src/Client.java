import HumanStructure.Human;
import HumanStructure.Man;
import HumanStructure.Woman;

public class Client {

	public static void main(String[] args) {
		Man man1 = new Man("John doe");
		// man1.Name = "John doe";
		man1.PrintFirstLine();
		man1.PrintSecondLine();
		
		// example of polymorphism
		Human h = new Man("mm");
		h.run();
		h.print();
		

		/*
		 * Woman woman1 = new Woman("Micheal"); //woman1.Name = "Micheal Obama";
		 * woman1.PrintFirstLine(); woman1.PrintSecondLine();
		 */

	}

}
