package HumanStructure;

public class Man extends Human {
	//private String Name = "mohona"; // optional
	public Man(String name) {
		super.Name = name;
	}

	public void PrintSecondLine() {
		System.out.println("He is a man");
	}
	
	@Override // example of polymorphism
	public void run()
	{
	System.out.println("car is not running"); 
	}
	
	private static void display() {
		System.out.println("Static or class method from Derived");
		}
	
	public void print() {
		System.out.println("Non-static or instance method from Derived");
		}

}
