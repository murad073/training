package HumanStructure;

public class Human {

	// public static void main(String[] args) {

	protected String Name;

	public void PrintFirstLine() {
		System.out.println("This is " + this.Name);
	}
	
	
	// example of polymorphism
	public void run()
	{
	System.out.println("car is running"); 
	}
	
	private static void display() {
		System.out.println("Static or class method from Base");
		}
	
	public void print() {
		System.out.println("Non-static or instance method from Base");
		}

	// }

}
