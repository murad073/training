// Class Declaration
public class Dog {
	
	    // Instance Variables
	    String breed;
	    String size;
	    int age;
	    String color;

	  
	    // method 1
	    public String getInfo() {
	        return ("Breed is: "+breed+" Size is:"+size+" Age is:"+age+" color is: "+color);
	    }
	    

	    public static void main(String[] args) {
	        Dog maltese = new Dog();
	        Dog dog2 = new Dog();
	        maltese.breed="Maltese";
	        maltese.size="Small";
	        maltese.age=2;
	        maltese.color="white";
	        System.out.println(maltese.getInfo());
	        dog2.breed="My own bread";
	        dog2.size="Giant";
	        dog2.age=12;
	        dog2.color="brown";
	        System.out.println(dog2.getInfo());
	        String info = dog2.getInfo();
	    }
	}
