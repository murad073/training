package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class LoginPage {
	
	public LoginPage(WebDriver driver) {
	PageFactory.initElements(driver, this);	
	}
	@FindBy(how = How.NAME, using = "UserName")
	public WebElement txtUserName;
	
	@FindBy(how = How.NAME, using = "Password")
	public WebElement txtPassword;
	
	@FindBy(how = How.NAME, using = "Login")
	public WebElement btnLogin;
	
	// page navigation concept
	
	public void Login(String userName, String password) {
		txtUserName.sendKeys(userName);
		txtPassword.sendKeys(password);
		
	}
	public void ClickLogin() {
		btnLogin.submit();
		
	}
	
		//base.Driver.findElement(By.name("UserName")).sendKeys(user.Username);
	//base.Driver.findElement(By.name("Password")).sendKeys(user.Password);
	//base.Driver.findElement(By.name("Login")).submit();
}
