package Argument_Transformation;

import cucumber.api.Transformer;
//import cucumberBasics.Steps.LoginSteps.User;

public class EmailTransform extends Transformer<String> {

	@Override
	public String transform(String username) {
		
		return username.concat("@ea.com");
	}

}
