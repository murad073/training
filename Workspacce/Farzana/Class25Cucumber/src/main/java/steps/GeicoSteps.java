package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.*;

public class GeicoSteps {
	
	private WebDriver _driver;
	private LandingPage _landingPage;
	private YourInfoPage _yourInfoPAge;
	
	public GeicoSteps() {
		// add something in system property
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\UserDB\\Documents\\Software\\chromedriver\\chromedriver_win32\\chromedriver.exe");
		this._driver = new ChromeDriver();
		this._driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	
	@Given("I navigated to geico website home page")
	public void Navigate_to_homepage() {
		this._driver.navigate().to("https://www.geico.com/");
		_landingPage = new LandingPage(this._driver);
		
	}
	
	@When("providing {int} as zip code")
	public void providing_zip_code (int zipCode) {
		/*WebElement element = this._driver.findElement(By.id("zip"));
		element.sendKeys(Integer.toString(zipCode));*/
		this._landingPage.EnterzipCode(Integer.toString(zipCode));
		
		
	}
	
	/*@When("providing {int} as zip code")
	public void providing_as_zip_code(Integer int1) {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}*/
	
	@And("clicking the start quote button")
	public void click_start_quote_button  () {
		
		/*WebElement element = this._driver.findElement(By.id("submitButton"));
		element.click();*/
		
		this._landingPage.SubmitButtinClick();
		// this._yourInfoPage = new YourInfoPage(this._driver);
	}
	
	@Then("I should see the users information page")
	public void Check_user_input_page  () {
		/*WebElement element = this._driver.findElement(By.id("auto-customer-collect-intent-modal"));
		Assert.assertNotNull(element);
		this._driver.quit();*/
		
		boolean inInYourPage = this._yourInfoPAge.IsInYourPage();
		Assert.assertTrue(inInYourPage);
		
		this._driver.quit();
		
	}
	

}
	