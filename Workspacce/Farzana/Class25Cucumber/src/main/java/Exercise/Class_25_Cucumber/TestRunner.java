package Exercise.Class_25_Cucumber;

/*import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;

@CucumberOptions(
features = "src/main/resources/features", glue = {"steps"})



public class TestRunner {
	
	// Step 1: Declared the runner variable
	private TestNGCucumberRunner testNGCucumberRunner = new TestNGCucumberRunner(TestRunner.class) ;
	
	 @BeforeClass(alwaysRun = true)
	   public void setUpClass() throws Exception {
	       testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
	   }
	
	// Step 2: Declared the feature test case
	@Test()
	   public void feature(CucumberFeatureWrapper cucumberFeature) {
	       testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
	   }
	

}*/
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests; //extends AbstractTestNGCucumberTests

@RunWith(Cucumber.class)
@CucumberOptions(features = ("src/main/resources/features"), glue = "steps")
public class TestRunner extends AbstractTestNGCucumberTests {
	
}