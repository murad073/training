package youTube_15_1;

public class Vigilante extends Villain {
	// Constructor +++++++++++++++++++++++++
	public Vigilante(String name) {
		super(name);
		// public methods +++++++++++++++++++++++++
	}
	
	public void dealsJustice() {
		System.out.println(this.name + "is dealing justice");
	}

}
