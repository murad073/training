package youTube_15_1;

public class StrongMan extends Hero {

	// Constructor ++++++++++++++++++++++
	public StrongMan(String name) {
		super(name);
		boosStrength();
	}
	
	// Private Methods ++++++++++++++++++++++++++++
		private void boosStrength() {
			this.strength += 50;
			if (this.strength > 100) {
				this.strength = 100;
			}
		}


}
