package youTube_15_1;

public class Hero extends Entity {

	// private properties+++++++++++++++++++++
protected int strength;
protected int speed;
private int health;

//  public properties +++++++++++++++++++++++++
public String name;

// getter section +++++++++++++++++++++++++++++++++

public int getStrength() {
	return strength;
}
public int getSpeed() {
	return speed;
}


public int getHealth() {
	return health;
}

// constructor++++++++++++++++++++++++++++
public Hero(String name) {
	this.name=name;
	generateAbilities();
}
public void showAbilities() {
	System.out.println("++++++++++++++++++++++++++++++++++++++");
	System.out.println("Strength: " + this.strength);
	System.out.println("Speed: " + this.speed);
	System.out.println("Health: " + this.health);
	System.out.println("++++++++++++++++++++++++++");
}


// Private methods+++++++++++++++++++++++++++++++
	private void generateAbilities() {
		this.strength =(int) (Math.random() * 100 +1);
		this.health =(int) (Math.random() * 100 +1);
		this.speed =(int) (Math.random() * 100 +1);
		
	}
	public void fight() {
		System.out.println(this.name+ " is fighting");
		
	}
	public void run() {
		System.out.println(this.name+ " is running");
	}
	
	
}
