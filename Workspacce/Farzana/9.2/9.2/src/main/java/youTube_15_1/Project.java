package youTube_15_1;

public class Project {

	public static void main(String[] args) {

		// instantiate hero class
		Hero hero = new Hero("Big man");
		hero.fight();
		hero.run();
		hero.showAbilities();

		// instantiate villain class extended hero class
		Villain vl = new Villain("Sly man");
		vl.steals(); // villain methods
		vl.kills(); // villain methods
		vl.fight();
		vl.run();
		vl.showAbilities();

		// instantiate vigilante class extended villain class
		Vigilante vg = new Vigilante("Ms. Savage");
		vg.dealsJustice(); // vigilante methods
		vg.steals();
		vg.kills();
		vg.fight();
		vg.run();
		vg.showAbilities();

		// instantiate strongMan class extended hero class
		StrongMan st = new StrongMan("Big Man");
		st.fight();
		st.run();
		st.showAbilities();

	}

}
