package youTube_15_1;

public class Villain extends Hero {

	// Constructor +++++++++++++++++++++++++
	public Villain(String name) {
		super(name);
	
	}

	// public methods +++++++++++++++++++++++++
	public void steals() {
		System.out.println(this.name + "is stealing");
	}

	public void kills() {
		System.out.println(this.name + "is killing");
	}
}
