package youTube_15_1;

public interface Action {
	
	// Define required actions
	public void fight();
	public void run();

}
