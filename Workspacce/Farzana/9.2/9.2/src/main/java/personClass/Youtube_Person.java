package personClass;

public class Youtube_Person {
	private String Name;
	private float height;
	private float weight;
	private int iq;
// constructor
	public Youtube_Person(String name, float weight, float height) {
		this.Name=name;
		this.height=height;
		//this.setHeight(6);
		this.weight = weight;
	}

	
	// getter and setter
	/*public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	} */

	public void walks() {
		this.weight *= 0.9;
		System.out.println(this.Name + " lost weight");
		System.out.println(this.Name +" weighs: " + this.weight + " lbs.");
	}

	public void eats() {
		this.weight *= 1.20;
		System.out.println(this.Name + " gained weight");
		System.out.println(this.Name + " weighs: " + this.weight + " lbs.");
	}

}
