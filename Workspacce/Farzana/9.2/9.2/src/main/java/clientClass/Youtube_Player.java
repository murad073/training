package clientClass;

import personClass.Youtube_Person;

public class Youtube_Player {

	public static void main(String[] args) {

		int year = 2018;
		System.out.println("Hello World!  Its's " + year + "!");

		Youtube_Person person1 = new Youtube_Person ("Tom", 225.3f, 6.0f);
		person1.eats();
		person1.walks();
		// tom.getHeight();
		//person1.setHeight(6);

		Youtube_Person person2 = new Youtube_Person("Mike", 189.9f, 6.5f);
		person2.walks();

	}

}
