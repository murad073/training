/// Question 7
public class problemSet4P {

	public static void main(String[] args) {
		/* int rows = 3;
		for (int i = 1; i <= rows; i++) {
		String thisRow = "";
		for (int j = 0; j < i; j++) {
		thisRow = thisRow + "#";
			
		}
		System.out.println(thisRow);
		
		} */
		//int result1= diceRoll( 6);
		int result2=monopolyRoll();
		//System.out.println(result1);
		System.out.println(result2);
	}
	
		
		/// Question 8
		
		 public static int diceRoll(int sides) {
			//This expression generates a random double in the interval
			//[0, sides).
			double randomNumber = Math.random() * sides;
			//Our random number is now in the interval [1, sides + 1)
			randomNumber = randomNumber + 1;
			//Casting the random number to an integer will round it down
			//to an integer in the 1 to sides range.
			return (int) randomNumber;
			}
			public static int monopolyRoll() {
			int roll1 = diceRoll(6);
			int roll2 = diceRoll(6);
			int total = roll1 + roll2;
			while (roll1 == roll2) {
			roll1 = diceRoll(6);
			roll2 = diceRoll(6);
			total = total + roll1 + roll2;
			}
			return total;
			} 
			
		

	//}

}
