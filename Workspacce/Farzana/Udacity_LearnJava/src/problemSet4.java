
public class problemSet4 {

	public static void main(String[] args) {
		
		// question 7
		
		int rows = 3;
		for (int i = 1; i <= rows; i++) {
		String thisRow = "";
		for (int j = 0; j < i; j++) {
		thisRow = thisRow + "#";
		}
		System.out.println(thisRow);
		}
	
		
		
		
		problemSet4 rd = new problemSet4();
		
		int matchingDice = rd.keepRolling();
		int mp = rd.monopolyRoll();
		int mp2 = rd.monopolyRoll2();
		int mp3 = rd.monopolyRoll2();
		
		System.out.println("To match 5 rolls needs count for " + matchingDice);
		System.out.println(mp);
		System.out.println(mp2);
		System.out.println(mp3);
	}

	public int rollDice(int sides) {
		// random num between 0 and (almost) 1
		double randomNumber = Math.random();

		// change range to 0 to (almost) 6
		randomNumber = randomNumber * sides;

		// shift range up one
		randomNumber = randomNumber + 1;

		// cast to integer (ignore decimal part)
		// ex. 6.998 becomes 6
		int randomInt = (int) randomNumber;

		// return statement
		return randomInt;
	}

	public int keepRolling() {
		int dice1 = rollDice(6);
		int dice2 = rollDice(6);
		int dice3 = rollDice(6);
		int dice4 = rollDice(6);
		int dice5 = rollDice(6);
		int count = 1;
		while (!(dice1 == dice2 && dice2 == dice3 && dice3 == dice4 && dice4 == dice5)) {
			// we need to re-roll
			dice1 = rollDice(6);
			dice2 = rollDice(6);
			dice3 = rollDice(6);
			dice4 = rollDice(6);
			dice5 = rollDice(6);
			count = count + 1;
		}
		return count;
	}
	
	//question 8

	public int monopolyRoll() {
		int roll1 = rollDice(6);
		int roll2 = rollDice(6);
		int total = roll1 + roll2;
		while (roll1 == roll2) {
			roll1 = rollDice(6);
			roll2 = rollDice(6);
			total = total + roll1 + roll2;
		}
		return total;

	}
	
	//question 8 // challange 1
	
	public int monopolyRoll2() {
		int roll1 = rollDice(6);
		int roll2 = rollDice(6);
		int total = roll1 + roll2;
		//An extra variable is added to keep track of how many rolls
		//have been made.
		int rollsSoFar = 1;
		while (roll1 == roll2) {
		//Here, we return -1 if doubles have been rolled too
		//many times in a row.
		if (rollsSoFar >= 3) return -1;
		roll1 = rollDice(6);
		roll2 = rollDice(6);
		total = total + roll1 + roll2;
		rollsSoFar = rollsSoFar + 1;
		}
		return total;
		}
	
	//question 8 // challange 2
	
	public int monopolyRoll3() {
		int roll1 = rollDice(6);
		int roll2 = rollDice(6);
		if (roll1 != roll2) {
		return roll1 + roll2;
		} else {
		//In the case where the two rolls are equal, we want to
		//return the current roll plus the return value of another
		//call to monopolyRoll(). This is called making a
		//recursive call. The recursive call will handle making
		//additional rolls, and recursive calls will keep getting
		//made until a roll is made where the two values are not
		//equal.
		return roll1 + roll2 + monopolyRoll3();
		}
		}
		
}
