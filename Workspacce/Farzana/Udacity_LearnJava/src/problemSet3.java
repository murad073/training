
public class problemSet3 {

	public static void main(String[] args) {
		problemSet3 callRollDice = new problemSet3();

		int RollDice1 = callRollDice.rollDice(10);
		
		int mp = callRollDice.monopolyRoll();

		System.out.println(RollDice1);
		System.out.println(mp);

	}

	/**
	 * Returns a random integer simulating a dice roll.
	 * 
	 * @param sides
	 *            Number of sides on the virtual die being rolled.
	 * @return random number in the range of 1 to sides.
	 */
	public int rollDice(int sides) {
		// random num between 0 and (almost) 1
		double randomNumber = Math.random();

		// change range to 0 to (almost) 6
		randomNumber = randomNumber * sides;

		// shift range up one
		randomNumber = randomNumber + 1;

		// cast to integer (ignore decimal part)
		// ex. 6.998 becomes 6
		int randomInt = (int) randomNumber;

		// return statement
		return randomInt;
	}

	public int monopolyRoll() {
		int roll1 = rollDice(6);
		int roll2 = rollDice(6);
		int total = roll1 + roll2;
		if (roll1 == roll2) {
			int roll3 = rollDice(6);
			int roll4 = rollDice(6);
			total = total + roll3 + roll4;
		}
		return total;

	}
}
