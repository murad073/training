import java.util.Scanner;

public class scannerTest {

	public static void main(String[] args) {
		
		String name;
		int age;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please type your name: ");
		name = input.nextLine();
		System.out.println("What is your age: ");
		age = input.nextInt();
		
		System.out.println("Hello "+name+". you are "+age+" years of old.");
	}

}
