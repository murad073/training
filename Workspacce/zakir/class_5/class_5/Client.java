package class_5;

import humanStructure.Man;
import humanStructure.Woman;

public class Client {

	public static void main(String[] args) {

		Man man1 = new Man("Zak");
		man1.PrintFirstLine();
		man1.PrintSecondLine();
		// man1.Name = "Zak";

		Woman woman1 = new Woman("Trina");
		// woman1.Name = "Trina";
		woman1.PrintFirstLine();
		woman1.PrintSecondLine();

	}
}