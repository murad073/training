package class_5;

	// Class Declaration
	public class Dog {
	    
		// Instance Variables
	    String breed;
	    String size;
	    int age;
	    String color;

	  
	    // method 1
	    public String getInfo() {
	        return ("Breed is: "+breed+" Size is:"+size+" Age is:"+age+" color is: "+color);
	    }
	    

	    public static void main(String[] args) {
	        Dog maltese = new Dog();
	        maltese.breed="Maltese";
	        maltese.size="Small";
	        maltese.age=2;
	        maltese.color="white";
	        
	        Dog chow = new Dog();
	        chow.breed="Chow Chow";
	        chow.size="Medium";
	        chow.age=3;
	        chow.color="Black";
	        System.out.println(chow.getInfo());
	        System.out.println(maltese.getInfo());
	    }
	}
