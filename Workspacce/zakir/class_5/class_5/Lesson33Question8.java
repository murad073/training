package class_5;

public class Lesson33Question8 {

	public static void main(String[] args) {
		System.out.println(monopolyRoll());
		}
	
	public static int randomInt() {
		double randomNumber = Math.random();
		randomNumber = randomNumber * 6;
		randomNumber = randomNumber + 1;
		
		int randNum = (int) randomNumber;
		return randNum;

	}
	public static int monopolyRoll() { 
		int roll1 = randomInt(); 
		int roll2 = randomInt(); 
		int total = roll1 + roll2; 
		while (roll1 == roll2) { 
			roll1 = randomInt(); 
			roll2 = randomInt(); 
			total = total + roll1 + roll2; 
			} 
		return total; 
			}	
}