package youtubeLesson;

import java.util.Random;
import java.util.Scanner;

public class whileLoop {

	public static void main(String[] args) {
		int max = 10;
		int i = 1;
		System.out.println("Counting up...");
		while (i<=max) {
			System.out.print(i+" ");
			i++;
		}
		System.out.println();
		
		i = max;
		System.out.println("Counting down...");
		while (i>=1) {
			System.out.print(i+" ");
			i--;
		}
		
		Scanner input = new Scanner(System.in);
		Random generator = new Random();
		int number = generator.nextInt(10) +1;
		int guess;
		int attempt = 1;
		System.out.println("I am thinking about a number between 1 and 10, What is it??????");
		/*while(guess != number) {
			System.out.print("Attempt "+attempt+ " : Your Guess->> ");
			guess = input.nextInt();
			attempt++;
		}
		System.out.println("Finally you guessed it. It was "+guess);
		*/
		do {
			System.out.print("Attempt "+attempt+ " : Your Guess->> ");
			guess = input.nextInt();
			attempt++;
		}while (guess!=number);
		System.out.println("Finally you guessed it. It was "+guess);
	}

}
