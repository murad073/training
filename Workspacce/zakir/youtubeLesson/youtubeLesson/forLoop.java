package youtubeLesson;

import java.util.Scanner;

public class forLoop {

	public static void main(String[] args) {
		int max = 9;
		System.out.println("Counting up...");
		for (int i=1; i <= max; i++) {
			System.out.printf("%d ",i);
		}
		System.out.println();
		
		int maxMultiples = 5;
		Scanner input = new Scanner (System.in);
		System.out.println("Enter the numer you want multiple of: ");
		int multiplesOf = input.nextInt();
		for (int i = 1; i<=maxMultiples; i++) {
			int answer= i*multiplesOf;
			System.out.print(i + " * "+multiplesOf+" == "+answer+"\n");
		}
		
		for (int j = 0; j <= max; j++) {
			for (int k = 0; k<=max; k++) {
				System.out.print("[ "+j+" "+k+" ]");
			}
			System.out.println();
		}
	}

}
