package Quizpractice;

public class SuperClass {

	String name;

	SuperClass() {
		System.out.println("Constructor method called.");
	}

	SuperClass(String t) {
		name = t;
	}

	public static void main(String[] args) {
		SuperClass cpp = new SuperClass();
		SuperClass java = new SuperClass("Java");
		java.getName();

	}

	void setName(String t) {
		name = t;
	}

	void getName() {
		System.out.println("Language name: " + name);
	}

}
