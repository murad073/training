package Interview.Practice;

import java.util.Arrays;
import java.util.Collections;
import org.apache.commons.*;
import org.apache.commons.lang3.ArrayUtils;

public class App {
	// public static void main(String[] args) {
	// String test = "my test class for interview";
	// String [] sp = test.split(" ");
	// ArrayUtils.reverse(sp);
	// System.out.println(String.join(" ", sp));
	// }

	/*
	 * public static void main(String[] args) { String test =
	 * "my test class for interview"; char [] sp = test.toCharArray();
	 * ArrayUtils.reverse(sp); System.out.println(String.valueOf(sp)); }
	 */
	public static void main(String[] args) {
//		String test = "class";
//		char[] sp = test.toCharArray();
//		for (int i = 0; i < sp.length / 2; i++) {
//			char temp = sp[i];
//			sp[i] = sp[sp.length - 1 - i];
//			sp[sp.length - 1 - i] = temp;
//		}
//		System.out.println(String.valueOf(sp));
		String test = "class";
		StringBuffer sbuffer = new StringBuffer();
		sbuffer.append(test);
		sbuffer.reverse();
		System.out.println(sbuffer);
		
		
	}
}
