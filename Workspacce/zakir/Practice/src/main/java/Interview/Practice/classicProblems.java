package Interview.Practice;

import java.util.*;

import org.apache.commons.lang3.ArrayUtils;

public class classicProblems {

	public static void main(String[] args) {
		int[] firstArray = {1,2,3,4,5,6,7};
		int lastDigits = 5;
		int[] newArray1;
		int[] newArray2;
		newArray1 = Arrays.copyOfRange(firstArray, 4, 7);
		newArray2 = Arrays.copyOfRange(firstArray, 0, 4);
		System.out.println(Arrays.toString(ArrayUtils.addAll(newArray1, newArray2)));
		for(int i=0; i<lastDigits; i++) {
			for (int j=firstArray.length-1; j>0; j--) {
				int temp = firstArray[j];
				firstArray[j] = firstArray[j-1];
				firstArray[j-1] = temp;
			}
		}
		System.out.println(Arrays.toString(firstArray));
//
//		for(int i=0; i<1; i++) {
//			for(int j=firstArray.length-1; j>5; j--) {
//				int temp = firstArray[j];
//				firstArray[j] = firstArray[j-1];
//				firstArray[j-1] = temp;
//			}
//		}
//		System.out.println(Arrays.toString(firstArray));
		
//		String newString = "the sky is light blue";
//		String[] stringArray = newString.split(" ");
//		for(int i=0; i<stringArray.length/2; i++) {
//			String temp = stringArray[i];
//			stringArray[i] = stringArray[stringArray.length-1-i];
//			stringArray[stringArray.length-1-i] = temp;			
//		}
//		System.out.println(String.join(" ", stringArray));
//		String newString = "This is a Test";
//		String[] stringArray = newString.split(" ");
//		String s = stringArray[stringArray.length-1];
//		System.out.println("Last word is: "+s+" and length is: "+s.length());
//		System.out.println();
	}
}
