package Interview.Practice;

import java.util.*;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class array {

	public static void main(String[] args) {
//		char[] fromArray = {'h','e','l','l','o','z','a','k'};
//		char[] toArray = new char[3];
//		
//		toArray = java.util.Arrays.copyOfRange(fromArray, 5, 8);
//		System.out.println(toArray);
//		System.arraycopy(fromArray, 5, toArray, 0, 3);
//		System.out.println(toArray);
		
//		int[] intArray = {1,2,5,4,6,4};
//		String printIntArray = Arrays.toString(intArray);
//		System.out.println(printIntArray);
//		
//		String[] stringArray = {"r","g","y","p"};
//		ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(stringArray));
//		System.out.println(arrayList);
		
//		String[] stringArray = {"a","d","f","r"};
//		boolean b = Arrays.asList(stringArray).contains("D");
//		System.out.println(b);
		
//		int[] intArray1 = {1,34,54,23};
//		int[] intArray2 = {45,46,23};
//		
//		int[] combinedArray = ArrayUtils.addAll(intArray1, intArray2);
//		System.out.println(Arrays.toString(combinedArray));
		
//		char[] method=(new char[] {'n','d','e'});
//		System.out.println(Arrays.toString(method));
		
//		String str1 = StringUtils.join(new String[] {"as","df"},"");
//		System.out.println(str1);
		
//		String[] stringArray = {"This","is","a","test"};
//		ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(stringArray));
//		String[] newStringArray = new String[arrayList.size()];
//		arrayList.toArray(newStringArray);
//		
//		for(String printString : newStringArray) {
//			System.out.println(printString);
//		}
//		String[] stringArray = {"a","b","c","d","e"};
//		System.out.println(Arrays.asList(stringArray));
//		System.out.println(Arrays.deepToString(stringArray));
//						
//		System.out.println(ArrayUtils.toString(stringArray));
//		Set<String> set = new HashSet<String>(Arrays.asList(stringArray));
//		
//		ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(stringArray));
//		ArrayUtils.reverse(stringArray);
//		System.out.println(arrayList);
//		System.out.println(set);
//		System.out.println(String.join(" ", stringArray));
//		
//		int[] intArray = {1,2,3,4,5};
//		int[] newIntArray = ArrayUtils.remove(intArray, 0);
//		System.out.println(Arrays.toString(newIntArray));
		
//		String test = "this is a test";
//		String[] test2 = test.split(" ");
//		
//		for(String s : test2) {
//			
//			System.out.println(s);
//		}
		
//		String join = StringUtils.join(new String[] {"This", "is", "a", "test"}, ",");
//		System.out.println(join);
//		int[] intArray = {2,3,4,5,6};
//		System.out.println(Arrays.toString(ArrayUtils.remove(intArray, 0)));
		
		String newString = "This is a test";
		String[] stringArray = newString.split(" ");
		ArrayUtils.reverse(stringArray);
//		System.out.println(Arrays.toString(stringArray));
		System.out.println(String.join(" ", stringArray));
		
//		String newString = "This is a test";
//		String[] stringArray = newString.split(" ");
//		
//		for( int i=0; i<stringArray.length/2; i++) {
//			String temp = stringArray[i];
//			stringArray[i] = stringArray[stringArray.length-1-i];
//			stringArray[stringArray.length-1-i] = temp;
//		}
//		System.out.println(String.join(" ", stringArray));
		
	}
}
