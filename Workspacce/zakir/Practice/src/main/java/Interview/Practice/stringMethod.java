package Interview.Practice;
import java.util.*;
import java.util.ArrayList;

import org.apache.commons.lang3.ArrayUtils;

public class stringMethod {

	public static void main(String[] args) {
		// String s = "New at home";
		// char c = s.charAt(10);
		// System.out.println(c);

		// String str1 = "Strings";
		// String str2 = new String ("strings");
		// String str3 = new String ("Strings");
		// String str4 = new String ("Integers");
		//
		// int result1 = str1.compareTo(str2);
		// System.out.println(result1);
		//
		// int result2 = str2.compareToIgnoreCase(str3);
		// System.out.println(result2);
		//
		// int result3 = str3.compareTo(str4);
		// System.out.println(result3);
		//
		// int result4 = str1.compareTo(str4);
		// System.out.println(result4);
		// String str1 = "santa";
		// String str2 = "atnas";
		// StringBuffer sBuffer = new StringBuffer(str2);
		// sBuffer.reverse();
		// boolean result = str1.contentEquals(sBuffer);
		// System.out.println(result);

		// char[] Str1 = {'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd'};
		// String Str2 = "";
		// Str2 = Str2.copyValueOf( Str1, 9, 10 );
		// System.out.println(Str2);
		// char[] Str1 = { 'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd' };
		// String Str2 = "";
		// Str2 = Str2.copyValueOf(Str1, 0, 5);
		// System.out.println(Str2);
		//
		// String Str3 = "";
		// Str3 = Str3.copyValueOf(Str1, 6, 5);
		// System.out.println(Str3);
		// boolean lastword;
		// String str = new String ("I like this");
		// lastword = str.startsWith("like", 2);
		// System.out.println(lastword);

		// String str1 = "I like this";
		// String str2 = "I LIKE this";
		// boolean match = str1.equalsIgnoreCase(str2);;
		// System.out.println(match);
		// String str = "I don't like this";
		// System.out.println(str.replaceFirst("i", "s"));
		// String str = "I like this";
		// for (String retval: str.split(" "))
		// System.out.println(retval);
		// String str = "I like it";
		// boolean match = str.startsWith("l");
		// System.out.println(match);
		// String str = "I like it";
		// System.out.println(str.substring(7));
		// String str = "I like it";
		//// char[] c = str.toCharArray();
		// System.out.println(str.toCharArray());
		//
		// String str = "I Like It";
		// System.out.println(str.toUpperCase());
		// String str = " I like it ";
		// System.out.println(str.trim());
		// int a = 1564;
		// float b = 155.55f;
		// char[] c = {'i',' ','l','i','k','e'};
		//
		// System.out.println(String.valueOf(a));
		// System.out.println(String.valueOf(b));
		// System.out.println(String.valueOf(c));
		// String str1 = "santa";
		// String str2 = "atnas";
		// StringBuffer sBuffer = new StringBuffer (str2);
		// sBuffer.reverse();
		// char[] c = str2.toCharArray();
		// for (int i=0; i<c.length/2; i++) {
		// char temp = c[i];
		// c[i] = c[c.length-1-i];
		// c[c.length-1-i] = temp;
		// }
		// String str3 = String.copyValueOf(c);
		// boolean match = str1.equals(str3);
		// System.out.println(match);
		// String str = "madama";
		// char[] c = str.toCharArray();
		// boolean isPalindrom = true;
		// for (int i = 0; i<c.length/2; i++) {
		// if (c[i]!=c[c.length-1-i])
		// isPalindrom = false;
		// }
		// System.out.println(isPalindrom);

		// String s1 = "Hi ";
		// s1 = s1.concat("Zak");
		// System.out.println(s1);
		// String s1 = "";
		// String s2 = "Hi";
		// System.out.println(s1.isEmpty());
		// System.out.println(s2.isEmpty());
		// int a = 10;
		// int b =5;
		// String s1 = String.valueOf(a);
		// System.out.println(a+b+String.valueOf(b)+s1);
		// String s1 = "Hello!";
		// s1 = s1.replaceAll("[e, o]", "l");
		// System.out.println(s1);
		// String s1 = "This what i want!!";
		// s1 = s1.replace("want", "need");
		// System.out.println(s1.replace("want", "need"));
		// String s1 = "I like it";
		// System.out.println(s1.contains("don't"));
		// String s1 = "This is what i want";
		// char[] charArray = s1.toCharArray();
		// for (int i=0; i<charArray.length/2; i++) {
		// if (charArray[i] == 'i') {
		// charArray[i] = 'a';
		// }
		// }
		// System.out.println(charArray);
		// String s1 = "This is what i want";
		// System.out.println(s1.replaceFirst("i", "a"));
		// System.out.println(s1.replace("want", "wint"));
		// String s1 = "This is what i Want";
		// String s2 = "THIS IS WHAT I WANT";
		// System.out.println(s1.endsWith("t"));
		// System.out.println(s1.endsWith("Want"));
		// System.out.println(s2.compareToIgnoreCase(s1));
		// System.out.println(s1.endsWith("me"));

		String[][] names = { { "University:", "Name:", "Subject:", "ID:", "Student?" },
				{ "Zakir", "Azad", "Opu", "Anik", "Wafee" }, { "WUV", "Newtrok", "Yes", "No", null },
				{ "22", "25", "29", null } };
		String[] forStudent1 = { names[0][0] + names[2][0], names[0][1] + names[1][0], names[0][2] + names[2][1],
				names[0][3] + names[3][0], names[0][4] + names[2][2] };
		String[] forStudent2 = { names[0][0] + names[2][0], names[0][1] + names[1][1], names[0][2] + names[2][1],
				names[0][3] + names[3][1], names[0][4] + names[2][2] };
		String[] forStudent3 = { names[0][0] + names[2][0], names[0][1] + names[1][4], names[0][2] + names[2][4],
				names[0][3] + names[3][3], names[0][4] + names[2][3] };
		String[] forStudent4 = { names[0][0] + names[2][0], names[0][1] + names[1][2], names[0][2] + names[2][4],
				names[0][3] + names[3][3], names[0][4] + names[2][3] };
		int count = 0;

		String student1 = String.join(" ", forStudent1);
		String student2 = String.join(" ", forStudent2);
		String student3 = String.join(" ", forStudent3);
		String student4 = String.join(" ", forStudent4);
//		 char[] nChar1 = student1.toCharArray();
//		 char[] nChar2 = student2.toCharArray();
//		 char[] nChar3 = student3.toCharArray();
//		 char[] allStudentChar = new char[] [] {{nChar1},{nChar2},{nChar3}};

		String[] allStudent = { student1, student2, student3, student4 };

		for (int i = 0; i < allStudent.length; i++) {
			if (allStudent[i].contains("null"))

				count++;
		}
		System.out.println("Count: " + count + " ");
//		System.out.println(String.join("\n", allStudent));
		System.out.println(String.join("\n",(allStudent)));
		System.out.println(ArrayUtils.contains(names, "WUV"));
		
	}
}
