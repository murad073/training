package Interview.Practice;

public class App2 {

	public static void main(String[] args) {

		String s = "Red rum, sir, is murder";
		s = s.toLowerCase();
		s = s.replaceAll("[ ,]", "");
		
		System.out.println(s);

		boolean isPalindrome = false;

		for (int i = 0; i < s.length() / 2; i++) {
			if (s.charAt(i) == s.charAt(s.length() - 1 - i)) {
				isPalindrome = true;
			}
		}
		System.out.println(isPalindrome);

	}
}
