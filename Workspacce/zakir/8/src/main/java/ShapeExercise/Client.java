package ShapeExercise;
import java.util.*;
import java.awt.List;

public class Client {

	public static void main(String[] args) {
		
//		List list = new ArrayList();
		Client c = new Client();
		
//		list.add(x.createShape("circle"));
//		list.add(x.createShape("rectangle"));
//		list.add(x.createShape("square"));
//		Client c = new Client();
		
		Shape shape1 = c.createShape("circle");
		Shape shape2 = c.createShape("rectangle");
		Shape shape3 = c.createShape("square");
		
		System.out.println(shape1.getArea());
		System.out.println(shape2.getArea());
		System.out.println(shape3.getArea());

		
	}
		public Shape createShape(String type) {
			Shape result = null;
			if (type == "circle") {
				result = new Circle(2);
		}else if (type == "rectangle") {
			result = new Rectangle(4,6);
			}else if (type == "square") {
				result = new Square(4);
			}
			return result;
	}

}

