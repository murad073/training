package ShapeExercise;

public class Circle extends Shape {
	private int radius;
	
	public Circle(int radius) {
		this.radius = radius;
	}
	@Override
	public double getArea() {
		double area = 3.1416 * this.radius * this.radius;
		return area;
	}
}
