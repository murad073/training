import java.util.Arrays;

public class App1 {

	public static void main(String[] args) {
		// two int array any size, return a new array with sum of those arrays with same index
		int[] a = new int[] {};	
		int[] b = new int[] {5, 6, 7, 8, 1, 5, 50};
		int maxLength = a.length > b.length ? a.length : b.length;
		
		int[] result = new int[maxLength];
		
		for(int i=0; i<maxLength; i++) { 
			
			if(i < a.length) { 
				result[i] += a[i];
			}
			
			if(i < b.length) { 
				result[i] += b[i];
			}
		}
		
		System.out.println( Arrays.toString(result) );
	}

}
