
public class App2 {

	public static void main(String[] args) {
		
		String s = "this is a test song";
		
		String[] parts = s.split(" ");
		
		for(int i=0; i < parts.length/2; i++) 
		{
			String temp = parts[i];
			parts[i] = parts[parts.length-1-i];
			parts[parts.length-1-i] = temp;
		}
		
		// [this, is, a, test, song]
		// 
		
		System.out.println( String.join(" ", parts) );
	}

}
