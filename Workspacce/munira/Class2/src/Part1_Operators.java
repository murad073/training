
public class Part1_Operators {

	public static void main(String[] args) {
		int num1 = 20;
		int num2 = 5;
		System.out.println(&quot;num1 / num2: &quot; + (num1 / num2) + &quot; and &quot; + &quot;num1 % num2: &quot; +
		(num1 % num2));
		num2 = num1;
		num1 *= num2;
		System.out.println(&quot;Now num2 value is &quot; + num2 + &quot; and num1 value is &quot; + num1);
		num1++;
		num2-- ;
		System.out.println(&quot;Now num1 value is &quot; + num1 + &quot; and &quot; + &quot;num2 value is &quot;
		+num2);
		boolean b1 = num1 &gt; num2;
		boolean b2 = false;
		System.out.println(&quot;Boolean value b1 is &quot; + b1 + &quot; and &quot; + &quot;b2 is &quot; + b2);
		System.out.println(&quot;Boolean value (b1 &amp;&amp; b2) is &quot; + (b1 &amp;&amp; b2) + &quot; and &quot; + &quot;(b1 || b2) is &quot;
		+ (b2 || b1) + &quot; and &quot; + &quot;!(b1 &amp;&amp; b2) is &quot; + !(b1 &amp;&amp; b2));

		boolean b3 = num1 == num2;
		boolean b4 = num2 &lt;= num1;
		System.out.println(&quot;Boolean value b3 is &quot; + b3 + &quot; and &quot; + &quot;b4 is &quot; + b4);// TODO Auto-generated method stub

	}

}
