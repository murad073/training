package ShapeExercise;

public class Square extends Shape {
	private int side;
	
	
	public Square(int side) {
		this.side = side;
	}
		
		
	@Override
	public double getArea() {
		int area = this.side * this.side;
		return area;
	}

}
