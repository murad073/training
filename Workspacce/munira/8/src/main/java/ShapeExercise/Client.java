package ShapeExercise;

public class Client {

	public static void main(String[] args) {
		
		Client client = new Client();
	
      Shape shape1 = client.createShape("circle");
      Shape shape2 = client.createShape("rectangle");
      Shape shape3 = client.createShape("square");
	
      System.out.println(shape1.getArea());
	System.out.println(shape2.getArea());
	System.out.println(shape3.getArea());
	}
	
	public Shape createShape(String type){
		
		Shape result = null;
		
		if(type == "circle") {
			result = new Circle(5);
		} else if (type == "rectangle") {
			result = new Rectangle(4,6);
		} else if (type == "square") { 
			result = new Square (4);
		}
		
	

     return result;
}
}
