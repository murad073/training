package ShapeExercise;

public  abstract class Shape {
    public abstract double getArea();
}
