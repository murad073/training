import java.util.Scanner;

public class ScannerTest {

	public static void main(String[] args) {
		System.out.println("Enter your name: ");
		Scanner S = new Scanner(System.in);
		String name = S.nextLine();
		System.out.println(name);
		System.out.println("Enter your number: ");
		Scanner number = new Scanner(System.in);
		int num = number.nextInt();
		
		System.out.println(num);

	}

}
