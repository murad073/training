package ShapeExercise;

import java.util.*;

public class Client {

	public static void main(String[] args) {
		
		List list=new ArrayList();
		Client c =new Client();
		
		list.add(c.createShape("circle"));
		list.add(c.createShape("rectangle"));
		list.add(c.createShape("square"));
		
		for(int i=0; i<list.size(); i++) {
			System.out.println(((Shape) list.get(i)).getArea());
		}
		
		
		//Shape shape1=c.createShape("circle");
		//Shape shape2=c.createShape("rectangle");
		//Shape shape3=c.createShape("square");
		
		//System.out.println(shape1.getArea());
		//System.out.println(shape2.getArea());
		//System.out.println(shape3.getArea());
	}

public Shape createShape(String type) {
	Shape result= null;
	if(type=="circle") {
		result=new Circle(5);
	}
	else if(type=="rectangle") {
		result = new Rectangle(4,6);
	}
		else if (type=="square") {
			result=new Square(5);
		
	}
	
	return result;
}

}