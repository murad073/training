package Class;

public abstract class Person {
	private String _name;
	private String _address;
	public Person(String name, String address) {
	
		this._name = name;
		this._address = address;

}
	public String getName() {
		return _name;
	}

	public String getAddress() {
		return _address;
	}
	public void setAddress(String newAddress) {
	_address = newAddress;
	}
	
	public String toString() {

		return  "Name:"+_name + ", Address: " + _address ;

	}
}