package Class;

public class Staff extends Person{
	private String _school;
	private double _pay;

	public Staff(String name, String address, String school, double pay) {
		super(name, address);
		this._pay= pay;
		this._school= school;
    
    }
	public String getSchool() {
		return _school;
	}
	public void setSchool(String newSchool) {
		_school= newSchool;
	}
	public double getPay() {
		return _pay;
	}

	public void setPay(double newPay) {
		_pay= newPay;
	}
	@Override
	public String toString() {
		return "Staff[ person[ Name:  " +getName() +", Adress: "+ getAddress()+"]" 
				+ "School :"+ getSchool() +", Pay:"+ Double.toString(getPay()) + "]";
	}
}