package Class;

public class Student extends Person {
	private String _program;
	private int _year;
	private double _fee;
	public Student(String program, int year, double fee,String name, String address) {
	super(name, address);
		this._program=program;
		this._fee= fee;
		this._year=year;
	
}
public String getProgram() {
		
		return _program;
	}
	public void setProgram(String newProgram) {
		_program = newProgram;
	
	}
public int getYear() {
		
		return _year;
	}
public void setYear(int newYear) {
	
	_year= newYear;
}
public double getfee() {
	
	return _fee;
}
public void setfee( double newFee) {
	
	_fee= newFee;
}
@Override
public String toString() {
    return "Student[ person[ Name:  " +getName() +", Address: "+ getAddress()+"]" 
+ "Program :"+ getProgram() +",Year:"+ String.valueOf(getYear()) +", Fee:"+ Double.toString(getfee()) + "]";
}
}