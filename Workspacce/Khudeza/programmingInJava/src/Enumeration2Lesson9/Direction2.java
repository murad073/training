package Enumeration2Lesson9;

public enum Direction2 {
	North(0), South(180), East(90), West(270);
	private final int degrees;
	Direction2(int degrees){
		this.degrees=degrees;
	}
    public int Degrees() {
    	return this.degrees;
    }



}
