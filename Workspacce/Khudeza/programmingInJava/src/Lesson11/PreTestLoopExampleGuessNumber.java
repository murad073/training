package Lesson11;

import java.util.Random;
import java.util.Scanner;

public class PreTestLoopExampleGuessNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        Scanner input=new Scanner(System.in);
        Random generator=new Random();
        int number=generator.nextInt(10)+1;
        int guess=0;
        int attempt=1;
        System.out.println("I'm thinking of a number between 1 and 10. What is it?!?!?");
        while(guess !=number) {
        	System.out.printf("Attend %d : your guess==>", attempt);
        	guess=input.nextInt();
        	attempt++;
        }
        System.out.printf("Finally! You guessed it. It was %d,\n", guess);
	}

}
