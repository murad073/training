package Lesson10;

public class lesson10BottlesOfBeer {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
       for(int beers=99; beers>0; beers--) {
    	   System.out.printf("%d bottles of beer on the wall, ", beers);
    	   System.out.printf("%d bottles of beer. If one of those bottles should happen to fall, ", beers);
    	   System.out.printf("%d bottles of beer on the wall!\n ", beers-1);
       }
	}

}
