//for loop basics
package Lesson10;

public class lesson10ForLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      int max=10;
      System.out.println("Counting up...");
      for(int i =1; i<=max; i++) {
    	  System.out.printf("%d ",i);
      }
      System.out.println();
	}

}
