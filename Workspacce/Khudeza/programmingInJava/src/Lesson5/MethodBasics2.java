package Lesson5;

import java.util.Scanner;

public class MethodBasics2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int length, width;
		Scanner input=new Scanner(System.in);
		System.out.print("Enter the length: ");
		length=input.nextInt();
		System.out.print("Enter the width: ");
		width=input.nextInt();
		int area=rectangleArea(length,width);
		int per=rectanglePerimeter(length,width);
		System.out.printf("Rectangle area:%d; perimeter:%d;\n", area,per);

	}

	public static int rectangleArea(int somelength, int somewidth) {
		return somelength*somewidth;
	}
	public static int rectanglePerimeter(int somelength, int somewidth) {
		return 2*(somelength*somewidth);
	}
}
