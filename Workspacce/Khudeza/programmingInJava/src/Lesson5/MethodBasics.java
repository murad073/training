package Lesson5;

public class MethodBasics {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        System.out.println(rectangleArea(5,10));
        System.out.println(rectangleArea(7,6));
        System.out.println(rectangleArea(3,1));
	}
	public static int rectangleArea(int somelength, int somewidth) {
		return somelength*somewidth;
	}
	public static int rectanglePerimeter(int somelength, int somewidth) {
		return 2*(somelength*somewidth);
	}

}
