
public class problem8 {

	public int monopolyRoll(){
		int roll1 = rollDice(6);
		int roll2 = rollDice(6);
		int total=roll1+roll2;
		
		while(roll1==roll2) {
			rollDice(6);
			rollDice(6);
			total=total+rollDice(6)+rollDice(6);
		}
		return total;
		

	}
    public int rollDice(int n) {
    	double randomNumber=Math.random();
    	randomNumber *=n;
    	randomNumber +=1;
    	
    	return(int) randomNumber;
    }
}
