package ShapeExercise;

public class Rectangle extends Shape{
	private int length;
	private int width;
    public Rectangle(int length,int width){
		this.length=length;
		this.width=width;
	}
	@Override
	public double getArea() {
		// TODO Auto-generated method stub
		int area=this.length*this.width;
		return area;
	}

}
