package ShapeExercise;

public class Circle extends Shape{
	private int radius;
	
    public Circle(int radius) {
    	this.radius=radius;
    }
    
	@Override
	public double getArea() {
		// TODO Auto-generated method stub
		double area=3.1416*this.radius*this.radius;
		return area;
	}
	
  
}
