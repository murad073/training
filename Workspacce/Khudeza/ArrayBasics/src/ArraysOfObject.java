
public class ArraysOfObject {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student[] students = new Student[] { new Student("Tom", 77), new Student("Ed", 84), new Student("Joe", 80),
				new Student("Bob", 96)

		};
		System.out.printf("#\tStudent\tGrade\n");
		System.out.printf("\t       \t     \n");

		for (int i = 0; i < students.length; i++) {
			System.out.printf("%d\t%s\t%d\n", i, students[i].Name(), students[i].Grade());

		}
	}

}
