import HumanStructure.Woman;
import HumanStructure.Man;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Man man1 = new Man("John Doe");
		// man1.Name = "John Doe";

		man1.PrintFirstLine();
		man1.PrintSecondLine();

		Woman woman1 = new Woman("Julia Robert");
		// Woman woman1 = new Woman();//or Human woman1= new Woman();
		// woman1.Name = "Julia Robert";//woman1.Name = "Julia Robert";
		// woman1.PrintFirstLine();
		woman1.PrintFirstLine(); // woman1.PrintSecondLine(); //it will give wrong
		woman1.PrintSecondLine();

	}

}
