package Exercise.class18;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
    
    public static void Swap(long num1, long num2) {
    	long temp = num1;
    	num1 = num2;
    	num2 = temp;
    }

	public static long Sum(long a, long b) {
		return a + b;
	}
}
