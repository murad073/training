package Exercise.class18;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
	@Test
	public void Sum_two_long() {

		long a = 10;
		long b = 20;
		App.Swap(a, b);
		Assert.assertEquals(20, a);
		Assert.assertEquals(10, b);

		// long a = 10;
		// long b = 20;
		// long c = App.Sum(a, b);
		// Assert.assertEquals(30, c);
	}
}
