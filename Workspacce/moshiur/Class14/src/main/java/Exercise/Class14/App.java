package Exercise.Class14;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;

/**
 * This class is used for file read purpose
 *
 */
public class App {

	public static void main(String[] args) throws IOException {
		// Using built in java io 
		// File file1 = new File("C:\\Users\\murad\\Downloads\\movieList.txt");
		// Scanner scan = new Scanner(file1);
		// scan.useDelimiter("\\Z");
		// System.out.println(scan.next());

		// Using Apache common library 
		// File file1 = new File("C:\\Users\\murad\\Downloads\\movieList.txt");
		// System.out.println( FileUtils.readFileToString(file1) );

		// Read file from resources 
		ClassLoader loader = App.class.getClassLoader();
		File file = new File(loader.getResource("users.json").getFile());
		Scanner scan = new Scanner(file);
		scan.useDelimiter("\\Z");
		System.out.println(scan.next());

	}
}
