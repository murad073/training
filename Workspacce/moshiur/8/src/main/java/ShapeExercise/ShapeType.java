/**
 * 
 */
package ShapeExercise;

/**
 * @author Moshiur
 *
 */
public enum ShapeType {
	Circle, Triangle, Square, Rectangle;
	
	private ShapeType() {
		System.out.println(this); 
	}
}
