package ShapeExercise;

import java.util.*;

public class Client {

	public static void main(String[] args) {
		List list = new ArrayList();
		Client x = new Client();

//		list.add(x.createShape("circle"));
//		list.add(x.createShape("rectangle"));
//		list.add(x.createShape("square"));
//		
//		for	(int i=0; i < list.size(); i++) { 
//			 System.out.println(((Shape)list.get(i)).getArea());
//		}

		 Shape shape1 = x.createShape(ShapeType.Circle);
		// Shape shape2 = x.createShape("rectangle");
		 Shape shape3 = x.createShape(ShapeType.Rectangle);
		 
		 System.out.println(shape1.getPerimeter());
		 System.out.println(shape3.getPerimeter());

		// System.out.println(shape1.getArea());
		// System.out.println(shape2.getArea());
		// System.out.println(shape3.getArea());
	}

	public Shape createShape(ShapeType type) {

		Shape result = null;

		if (type == ShapeType.Circle) {
			result = new Circle(5);
		} else if (type == ShapeType.Rectangle) {
			result = new Rectangle(4, 6);
		} else if (type == ShapeType.Square) {
			result = new Square(5);
		} 

		return result;
	}

}
