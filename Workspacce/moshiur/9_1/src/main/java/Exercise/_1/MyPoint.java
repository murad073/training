package Exercise._1;

public class MyPoint {

	private int x;
	private int y;

	public MyPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public double distance(MyPoint v2) {
		return Math.sqrt((v2.getX() - this.x) * (v2.getX() - this.x) + (v2.getY() - this.y) * (v2.getY() - this.y));
	}

}
