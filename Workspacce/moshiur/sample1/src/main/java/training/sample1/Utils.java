package training.sample1;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;

public class Utils {
	
	public static WebElement waitFindElement(WebDriver driver, By by) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		return driver.findElement(by);
	}
	
	public static WebElement clearSendKey(WebDriver driver, By by, String text) {
		WebElement element = waitFindElement(driver, by);
		element.clear();
		element.sendKeys(text);
		return element;
	}
	
	public static WebElement scrollToElement(WebDriver driver, By by)
	{
	    WebElement element = driver.findElement(by);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(" + element.getLocation().x + "," + (element.getLocation().y
				- 100) + ");");
	    return element;		
	 }
}

