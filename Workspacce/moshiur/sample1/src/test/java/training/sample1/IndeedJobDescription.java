package training.sample1;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class IndeedJobDescription {

	private WebDriver driver;

	public IndeedJobDescription() {
		// System.setProperty("webdriver.chrome.driver", "");
		WebDriverManager.chromedriver().setup();
		WebDriverManager.firefoxdriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	@Test
	public void test() {
		goToUrl();
		searchJob();
		tryRemovePopup();
		goToJob();
		findInDescription();
	}

	private void goToUrl() {
		driver.navigate().to("https://www.indeed.com/");
		waitForViewer();
	}

	private void searchJob() {
		Utils.clearSendKey(driver, By.id("what"), "Java Selenium");
		waitForViewer();
		Utils.clearSendKey(driver, By.id("where"), "Virginia");
		waitForViewer();
		Utils.waitFindElement(driver, By.id("fj")).click();
	}

	private void tryRemovePopup() {
		WebElement element = Utils.waitFindElement(driver, By.xpath("//div[contains(@id, \"popover-x\")]"));
		if (element != null) {
			element.click();
		}
	}

	private void goToJob() {
		By by = By.xpath("//span[@class=\"company\" and contains(., \"Techflow, Inc.\")]/../h2/a");
		WebElement element = Utils.waitFindElement(driver, by);
		waitForViewer();
		element = Utils.scrollToElement(driver, by);
		waitForViewer();
		element.click();
	}

	private void findInDescription() {

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));

		WebElement element = Utils.waitFindElement(driver, By.xpath("//p[contains(., \"Computer Science\")]"));
		waitForViewer();
		highlightElement(driver, element);

		waitForViewer();
		waitForViewer();
		driver.close();
		driver.switchTo().window(tabs2.get(0));
	}

	private void waitForViewer() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void highlightElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
	}
}
