package PracticeSingleTon.Class12;

public class StringOperations {

	public static void main(String[] args) {
		String sample = "This is a test! wow! meaww.";
		char[] chrs = sample.toLowerCase().toCharArray();
		
		int vowelCount = 0;
		int consonentCount = 0;
		
		for(int i = 0; i < chrs.length; i++) { 
			if (chrs[i] == 'a'  || chrs[i] == 'A'
				|| chrs[i] == 'e' || chrs[i] == 'E'
				|| chrs[i] == 'i' || chrs[i] == 'I'
				|| chrs[i] == 'o' || chrs[i] == 'O'
				|| chrs[i] == 'u' || chrs[i] == 'U') { 
				vowelCount++;
			} else if ( (chrs[i] > 'a' && chrs[i] <= 'z') || (chrs[i] > 'A' && chrs[i] <= 'Z') ) {
				consonentCount++;
			}
		}

		System.out.println("Vowel found: " + vowelCount);
		System.out.println("Consonent found: " + consonentCount);
	}

}
