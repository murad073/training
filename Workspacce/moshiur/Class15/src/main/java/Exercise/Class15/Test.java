/**
 * 
 */
package Exercise.Class15;

/**
 * @author Moshiur
 *
 */
public class Test {

	Day x;

	// Constructor
	public Test(Day day) {
		this.x = day;
	}

	// Prints a line about Day using switch
	public void dayIsLike() {
		switch (x) {
		case Monday:
			System.out.println("Mondays are bad.");
			break;
		case Friday:
			System.out.println("Fridays are better.");
			break;
		case Saturday:
		case Sunday:
			System.out.println("Weekends are best.");
			break;
		default:
			System.out.println("Midweek days are so-so.");
			break;
		}
	}

	// Driver method
	public static void main(String[] args) {
		Test t = new Test(Day.Tuesday);
		t.dayIsLike();
	}

}
