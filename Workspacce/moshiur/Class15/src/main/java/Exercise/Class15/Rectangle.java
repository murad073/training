/**
 * 
 */
package Exercise.Class15;

/**
 * @author Moshiur
 *
 */
public class Rectangle implements Drawable {
	@Override
	public void draw() {
		System.out.println("drawing rectangle");
	}
	
	@Override
	public void msg() { 
		System.out.println("overriding the default msg function");
	}
}
