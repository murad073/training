
package Exercise.Class15;

/**
 * 
 * @author Moshiur
 *
 */
public interface Drawable {
	void draw();

	default void msg() {
		System.out.println("default method");
	}
}
