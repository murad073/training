package Exercise.Class21;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestHelloWorld {
	
	@Test()
	public void testEmailGenerator() { 
		RandomEmailGenerator obj = new RandomEmailGenerator();
		String email = obj.generate();
		
		Assert.assertNotNull(email);
		Assert.assertEquals("murad073@gmail.com11", email); 
	}
}

