package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LandingPage {
	
	private WebDriver _driver;
	
	public LandingPage(WebDriver driver) { 
		PageFactory.initElements(driver, this);
		this._driver = driver;
	} 

	@FindBy(how=How.ID, using="zip")
	private WebElement _zipCodeTextBox;
	
	@FindBy(how=How.ID, using="submitButton")
	private WebElement _startQuoteButton;
	
	public void EnterZipCode(String zipCode) { 
		this._zipCodeTextBox.clear();
		this._zipCodeTextBox.sendKeys(zipCode); 
	}
	
	public void SubmitButtonClick() {
		this._startQuoteButton.click();
	}
}
