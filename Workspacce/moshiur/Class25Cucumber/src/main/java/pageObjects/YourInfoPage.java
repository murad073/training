package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class YourInfoPage {
	
	public YourInfoPage(WebDriver driver) { 
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how=How.LINK_TEXT, using="skip")
	private WebElement _skipLink;
	
	@FindBy(how=How.ID, using="firstName")
	private WebElement _firstNameTextBox;
	
	@FindBy(how=How.ID, using="lastName")
	private WebElement _lastNameTextBox;
	
	@FindBy(how=How.XPATH, using="//button[.=\"Next\"]")
	private WebElement _nextButton;
	
	public void SkipPopup() { 
		this._skipLink.click();
	}
	
	public boolean IsInYourInfoPage() { 
		return this._firstNameTextBox != null && this._firstNameTextBox.isDisplayed();
	}
}
