package Exercise.Class25Cucumber;

import org.testng.annotations.*;
import cucumber.api.*;
import cucumber.api.testng.*;

@CucumberOptions(features = "src/main/resources/features", glue = { "steps" })
public class TestRunner extends AbstractTestNGCucumberTests  {
	
    @DataProvider(parallel = true)
    @Override
    public Object[][] scenarios() {
        return super.scenarios();
    }
    
}
