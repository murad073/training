package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.testng.Assert;
import pageObjects.*;

import cucumber.api.java.en.*;

public class GeicoSteps {

	private WebDriver _driver;
	private LandingPage _landingPage;
	private YourInfoPage _yourInfoPage;

	public GeicoSteps() {
		// add something in system property
		System.setProperty("webdriver.chrome.driver",
				"C:\\training\\Workspacce\\moshiur\\Class25Cucumber\\src\\main\\resources\\drivers\\chromedriver.exe");
		this._driver = new ChromeDriver();
		this._driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@Given("I navigated to geico website home page")
	public void Navigate_to_homepage() {
		this._driver.navigate().to("https://www.geico.com/");
		_landingPage = new LandingPage(this._driver);
	}

	@When("providing {int} as zip code")
	public void Providing_zip_code(int zipCode) {
		this._landingPage.EnterZipCode(Integer.toString(zipCode));
	}

	@And("clicking the start quote button")
	public void Click_start_quote_button() {
		this._landingPage.SubmitButtonClick();
		this._yourInfoPage = new YourInfoPage(this._driver);
	}

	@Then("I should see the users information page")
	public void Check_user_input_page() {
		boolean isInYourInfoPage = this._yourInfoPage.IsInYourInfoPage();
		Assert.assertTrue(isInYourInfoPage); 
		
		this._driver.quit();
	}
}

