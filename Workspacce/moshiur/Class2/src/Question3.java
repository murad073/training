import java.util.*;

public class Question3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String name;
		int age;
		double gpa;
		
		System.out.print("Name? ");
		name = input.nextLine();
		System.out.print("Age? ");
		age = input.nextInt();
		System.out.print("GPA? ");
		gpa = input.nextDouble();
		
		System.out.println("Name='" + name + "' Age='" + age + "' GPA='" + gpa + "'");
	}

}
