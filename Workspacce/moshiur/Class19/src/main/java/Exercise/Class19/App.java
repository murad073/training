package Exercise.Class19;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.WebDriver.*;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\murad\\Desktop\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// implicit wait is for WebElement
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);		

		driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);		

		// Options x = driver.manage();
		// Timeouts t = x.timeouts();
		// t.implicitlyWait(15, TimeUnit.SECONDS);

		driver.navigate().to("https://www.indeed.com/");
		driver.manage().window().maximize();

		WebElement whatTextBox = driver.findElement(By.id("text-input-what"));
		WebElement findJobsButton = driver.findElement(By.xpath("//button[.='Find Jobs']")); // By.cssSelector(".icl-WhatWhere-button")

		whatTextBox.sendKeys("java selenium");
		findJobsButton.click();

		Thread.sleep(5000);

		driver.quit();
	}
}
