
public class ForLoopExercise {

	public static void main(String[] args) {

		for (int i = 1; i <= 25; i++) {

			// if the number is between 1 and 9, print a 0
			if (i <= 9) {
				System.out.print("0" + i + " ");
			} else {
				System.out.print(i + " ");
			}

			if (i % 5 == 0) {
				System.out.println();
				// System.out.print("\n");
			}
		}
	}
}
