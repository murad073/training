
public class Dog {
	// Instance Variables
	String breed;
	String size;
	int age;
	String color;

	// method 1
	public String getInfo() {
		return ("Breed is: " + breed + " Size is:" + size + " Age is:" + age + " color is: " + color);
	}

	public static void main(String[] args) {
		Dog maltese = new Dog();
		Dog dog2 = new Dog();
		
		maltese.breed = "Maltese";
		maltese.size = "Small";
		maltese.age = 2;
		maltese.color = "white";
		
		dog2.breed = "MyOwnBreed";
		dog2.size = "Giant"; 
		dog2.age = 90;
		dog2.color = "Black";		
		
		maltese.size = "Tiny";
		String info = maltese.getInfo();
		System.out.println(info);
	}
}
