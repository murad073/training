package Exercise.Class20;

import java.util.Random;

public class U2 extends Rocket {

	public U2() { 
		super.maxWeightLimit = 29 * 1000;
		super.currentWeight = 18 * 1000;
	}
	
	@Override
	public boolean land() { 
		double probability = 0.08 * super.currentWeight / super.maxWeightLimit;		
		double randomNumber = new Random().nextDouble() * 100 + 1;		
		return probability < randomNumber;
	}
	
	@Override
	public boolean launch() { 
		double probability = 0.04 * super.currentWeight / super.maxWeightLimit;		
		double randomNumber = new Random().nextDouble() * 100 + 1;		
		return probability < randomNumber;
	}
}
