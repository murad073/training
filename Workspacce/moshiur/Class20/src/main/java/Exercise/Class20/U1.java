package Exercise.Class20;

import java.util.Random;

public class U1 extends Rocket {

	public U1() { 
		super.maxWeightLimit = 18 * 1000;
		super.currentWeight = 10 * 1000;
	}
	
	@Override
	public boolean land() { 
		double probability = 0.01 * super.currentWeight / super.maxWeightLimit;		
		double randomNumber = new Random().nextDouble() * 100 + 1;		
		return probability < randomNumber;
	}
	
	@Override
	public boolean launch() { 
		double probability = 0.05 * super.currentWeight / super.maxWeightLimit;		
		double randomNumber = new Random().nextDouble() * 100 + 1;		
		return probability < randomNumber;
	}
}
