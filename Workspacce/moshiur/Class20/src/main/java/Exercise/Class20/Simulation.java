package Exercise.Class20;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Simulation {

	public ArrayList<Item> loadItems() throws IOException { 
		ClassLoader loader = getClass().getClassLoader();
		File file = new File(loader.getResource("phase-1.txt").getFile());
		// File file = new File("C:\\training\\Workspacce\\moshiur\\Class20\\src\\main\\resources\\phase-1.txt");
		
		List<String> allLines = Files.readAllLines(file.toPath());
		
		ArrayList<Item> allItems = new ArrayList<Item>();
		
		for (int i=0; i<allLines.size(); i++) { 
			String line = allLines.get(i);
			String[] parts = line.split("=");

			Item item = new Item();
			item.name = parts[0];
			item.weight = Integer.parseInt(parts[1]);
			
			allItems.add(item);
		}
		
		return allItems;
	}
	
	public void loadU1(ArrayList<Item> items) { 
		U1 u1Rocket = new U1();
	}
}

