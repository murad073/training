package Exercise.Class20;

public class Rocket implements SpaceShip {

	public int currentWeight;
	public int maxWeightLimit;
	
	public boolean launch() {
		return true;
	}

	public boolean land() {
		return true;
	}

	public boolean canCarry(Item item) {
		int approxWeight = this.currentWeight + item.weight;
		
		if (approxWeight <= this.maxWeightLimit) {
			return true;
		}
		return false;
	}

	public void carry(Item item) {
		this.currentWeight += item.weight;
	}
	
}
