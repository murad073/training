package training.temp;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;

public class UsingJacksonForJSON {

	public static void main(String[] args) throws Throwable {
		ObjectMapper mapper = new ObjectMapper();

		File file = new File( UsingJacksonForJSON.class.getClassLoader().getResource("info.json").getFile());
		EmployeePojo[] employees = mapper.readValue(file, EmployeePojo[].class);
		
		System.out.println(employees.length);
		
	}

}
