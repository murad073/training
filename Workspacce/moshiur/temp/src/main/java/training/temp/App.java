package training.temp;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

// https://mvnrepository.com/artifact/org.apache.directory.studio/org.apache.commons.io 
import org.apache.commons.io.FileUtils;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws Exception {

		// URL url = App.class.getResource("info.json");
		// File file = new File(url);

		// ClassLoader classLoader = App.class.getClassLoader();
		// File file = new File(classLoader.getResource("info.json").getFile());
		// String jsonText = FileUtils.readFileToString(file);
		// System.out.println(jsonText);

		 // System.out.println( App.class.getClassLoader().getResource("info.json").toURI() );

		 String s = new String(Files.readAllBytes(Paths.get(App.class.getClassLoader().getResource("info.json").toURI() )));
		 System.out.println(s);

		// Problem 1
		// String givenText = "AABBCDD";
		// String givenText = "AABBCCDEEFF";
		// System.out.println(firstNonRepeatingChar(givenText));

		// Problem 2
		// String givenText = "tree traversal";
		// System.out.println(replaceDuplicate(givenText));
	}

	// Problem 1
	// Write a function that takes a string " AABBCDD" and "AABBCCDEEFF", and finds
	// the first non-repeating character.
	public static char firstNonRepeatingChar(String givenText) {
		int length = givenText.length();
		char[] chs = givenText.toCharArray();

		if (length == 0)
			return 0;
		else if (length == 1)
			return chs[0];
		else if (length == 2 && chs[0] != chs[1])
			return chs[0];
		else {
			for (int i = 1; i < givenText.length() - 1; i++) {
				if (chs[i - 1] != chs[i] && chs[i] != chs[i + 1]) {
					return chs[i];
				}
			}
			if (chs[length - 1] != chs[length - 2]) {
				return chs[length - 1];
			}
		}

		return 0;
	}

	// Problem 2
	// Write a function that takes the string "tree traversal" and replaces all
	// duplicates with '!'.
	public static String replaceDuplicate(String givenText) {
		int length = givenText.length();
		char[] chs = givenText.toLowerCase().toCharArray();
		String newString = "";

		for (int i = 0; i < length; i++) {
			for (int j = 0; j < i; j++) {
				if (chs[i] == chs[j])
					chs[i] = 0;
			}
			if (chs[i] != 0) {
				newString += String.valueOf(chs[i]);
			}
		}

		return newString;
		// return String.valueOf(chs);
	}

}
