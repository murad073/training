package training.temp;

import java.io.*;
import java.net.*;
import java.nio.file.*;

import org.apache.commons.io.*;

public class UsingFileReader {

	public static void main(String[] args) throws Throwable {
		// Way 1
		// Path pathUri =
		// Paths.get(App.class.getClassLoader().getResource("info.json").toURI());
		// String s = new String(Files.readAllBytes(pathUri));
		// System.out.println(s);

		// Way 2
		// ClassLoader classLoader = App.class.getClassLoader();
		// File file = new File(classLoader.getResource("info.json").getFile());
		// String jsonText = FileUtils.readFileToString(file);
		// System.out.println(jsonText);

		// Way 3
		File file = new File("C:\\Users\\murad\\Desktop\\test2.js_rename");
		BufferedReader reader = new BufferedReader(new FileReader(file));
		
	}

}
