
public class Woman extends Human {
	public Woman(String name) {
		super(name);
	}

	public void PrintSecondLine() { 
		System.out.println("She is a woman.");
	}
}
