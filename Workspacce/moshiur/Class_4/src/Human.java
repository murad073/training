
public abstract class Human {
	private String name;
	
	public Human(String name) { 
		this.name = name;
	}
	
	public void PrintFirstLine() { 
		System.out.println("This is " + this.name + ".");
	}
	
	public abstract void PrintSecondLine();
	
	public void Print() { 
		PrintFirstLine();
		PrintSecondLine();
	}
}


