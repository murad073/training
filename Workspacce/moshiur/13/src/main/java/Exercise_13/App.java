package Exercise_13;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {    	
    	try {    		
        	int result = divide(5, 0); 
        	System.out.println(result);
    	} catch (Exception ex) {
    		System.out.println("Operation not valid - " + ex.toString());
    		try {
        		divide(4, 0);
    		} catch (NotAllowedZeroAsSecondParam error) {
    			System.out.println("Error Again!!");
    		}
    	} finally { 
    		System.out.println("I am in finally");
    	}
    }
    
    public static int divide(int num1, int num2) throws NotAllowedZeroAsSecondParam { 
    	if(num2 == 0 ) {
    		throw new NotAllowedZeroAsSecondParam();
    	}
    	return num1 / num2; 
    }
}
