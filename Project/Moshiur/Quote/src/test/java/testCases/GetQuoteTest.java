package testCases;

import org.testng.annotations.*;
import org.openqa.selenium.WebDriver;

import Geico.Quote.*;
import pageObjects.geicoPage;

import java.io.*;

public class GetQuoteTest {
	WebDriver _driver;
	ConfigFileReader _configReader;
	File _dataFile;
	TestDataReader _dataReader;

	@Factory(dataProvider = "ExcelTestDataFiles")
	public GetQuoteTest(File dataFile) {
		_dataFile = dataFile;

		_configReader = FileReaderManager.getInstance().getConfigReader();
		_driver = WebDriverManager.getInstance().getDriver();
		_dataReader = new TestDataReader(_dataFile);
	}
	
	@DataProvider(name = "ExcelTestDataFiles")
	public static Object[] dataFileName() {
		return FileReaderManager.getInstance().getDataFiles();
	}

	@BeforeClass(alwaysRun = true)
	public void setUpClass() throws Exception {
		_driver.navigate().to(_configReader.getApplicationUrl());
	}

	@Test
	public void a_AddCustomer() {
		geicoPage geicoPage = new geicoPage();
		String zip = _dataReader.getCustomerInfo("Zip code");
		geicoPage.enterZip(zip);
		geicoPage.ClickStartQuote();
	}

	@Test
	public void b_AddVehicles() {

	}

	@Test
	public void c_AddDrivers() {

	}

	@Test
	public void d_AddDetails() {

	}

	@AfterClass(alwaysRun = true)
	public void tearDownClass() throws Exception {
		WebDriverManager.getInstance().closeDriver();
	}
}
