package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class geicoPage extends BasePage {

	@FindBy(how = How.ID, using = "zip")
	public WebElement txt_zip;
	
	@FindBy(how = How.ID, using = "submitButton")
	public WebElement btn_submit;
	
	public void enterZip(String zip) { 
		txt_zip.clear();
		txt_zip.sendKeys(zip);
	}
	
	public void ClickStartQuote() { 
		btn_submit.click();
		// TODO: return next page 
	}
}
