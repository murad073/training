package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Geico.Quote.FileReaderManager;
import Geico.Quote.WebDriverManager;

public class BasePage {

	protected WebDriver _driver;
	protected String _baseUrl;

	public BasePage() {
		this._driver = WebDriverManager.getInstance().getDriver();
		PageFactory.initElements(this._driver, this);
		this._baseUrl = FileReaderManager.getInstance().getConfigReader().getApplicationUrl();
	}
}
