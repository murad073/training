package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class customerPage extends BasePage {

	@FindBy(how = How.ID, using = "firstName")
	public WebElement txt_firstName;

	@FindBy(how = How.ID, using = "lastName")
	public WebElement txt_lastName;

	@FindBy(how = How.ID, using = "date-monthdob")
	public WebElement txt_monthdob;

	@FindBy(how = How.ID, using = "date-daydob")
	public WebElement txt_daydob;

	@FindBy(how = How.ID, using = "date-yeardob")
	public WebElement txt_yeardob;

	@FindBy(how = How.XPATH, using = "//button[.='Next']")
	public WebElement btn_Next;

	public void enterFirstName(String firstName) {
		txt_firstName.clear();
		txt_firstName.sendKeys(firstName);
	}

	public void enterLastName(String lastName) {
		txt_lastName.clear();
		txt_lastName.sendKeys(lastName);
	}

	public void clickNext() {
		btn_Next.click();
	}

	public void enterDOB(String mm, String dd, String yyyy) {
		txt_monthdob.clear();
		txt_daydob.clear();
		txt_yeardob.clear();

		txt_monthdob.sendKeys(mm);
		txt_daydob.sendKeys(dd);
		txt_yeardob.sendKeys(yyyy);
	}
}
