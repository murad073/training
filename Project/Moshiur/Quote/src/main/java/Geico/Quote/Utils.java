package Geico.Quote;

import org.openqa.selenium.*;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {

	public static WebElement WaitScrollClick(WebDriver driver, By by) {
		WaitFindElement(driver, by);
		WebElement element = ScrollToView(driver, by);
		element.click();
		Wait(500);
		return element;
	}

	public static WebElement WaitScrollClick(WebElement element) {
		ScrollToView(element);
		element.click();
		Wait(500);
		return element;
	}

	public static WebElement WaitFindElement(WebDriver driver, By by) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
		return element;
	}

	public static WebElement WaitFindElement(WebDriver driver, By by, int seconds) {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
		return element;
	}

	public static WebElement ScrollToView(WebDriver driver, By by) {
		WebElement element = WaitFindElement(driver, by);
		return ScrollToView(driver, element);
	}

	public static WebElement ScrollToView(WebElement element) {		
		WebDriver driver = ((WrapsDriver)element).getWrappedDriver();
		return ScrollToView(driver, element);
	}

	private static WebElement ScrollToView(WebDriver driver, WebElement element) {		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		Wait(200);
		// Actions action = new Actions(driver);
		// action.moveToElement(element);
		// action.perform();
		// Wait(200);
		return element;
	}

	public static void Wait(long milisecond) { 
		try {
			Thread.sleep(milisecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
