package Geico.Quote;

import java.io.File;
import java.net.URL;

public class FileReaderManager {

	private static FileReaderManager fileReaderManager = new FileReaderManager();
	private static ConfigFileReader configFileReader;

	private FileReaderManager() {
	}

	public static FileReaderManager getInstance() {
		return fileReaderManager;
	}

	public ConfigFileReader getConfigReader() {
		String configFilePath = getResourceFilePath("config//Configuration.properties");
		return (configFileReader == null) ? new ConfigFileReader(configFilePath) : configFileReader;
	}

	public File getResourceFile(String resourceName) {
		ClassLoader loader = this.getClass().getClassLoader();
		File file = new File(loader.getResource(resourceName).getFile());
		return file;
	}
	
	public File[] getDataFiles() {
	    ClassLoader loader = Thread.currentThread().getContextClassLoader();
	    URL url = loader.getResource("data");
	    String path = url.getPath();
	    return new File(path).listFiles();
    }

	public String getResourceFilePath(String resourceName) {
		ClassLoader loader = this.getClass().getClassLoader();
		File file = new File(loader.getResource(resourceName).getFile());
		return file.getPath();
	}
}

