package Geico.Quote;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class WebDriverManager {
	private static WebDriver driver;
	private static DriverType driverType;
	private ConfigFileReader config;
	private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
	private static final String FIREFOX_DRIVER_PROPERTY = "webdriver.gecko.driver";
	private static final String IE_DRIVER_PROPERTY = "webdriver.ie.driver";
	private static WebDriverManager instance = new WebDriverManager();

	private WebDriverManager() {
		config = FileReaderManager.getInstance().getConfigReader();
		driverType = config.getBrowser();
	}
	
	public static WebDriverManager getInstance() { 
		return instance;
	}

	public WebDriver getDriver() {
		if(driver == null) driver = createLocalDriver();
		return driver;
	}

	private WebDriver createLocalDriver() {
        switch (driverType) {	    
        case FIREFOX : 
        	System.setProperty(FIREFOX_DRIVER_PROPERTY, FileReaderManager.getInstance().getResourceFilePath("drivers//geckodriver.exe"));
        	driver = new FirefoxDriver();
	    	break;
        case CHROME : 
        	System.setProperty(CHROME_DRIVER_PROPERTY, FileReaderManager.getInstance().getResourceFilePath("drivers//chromedriver.exe")); 
        	driver = new ChromeDriver();
    		break;
        case IE : 
        	System.setProperty(IE_DRIVER_PROPERTY, FileReaderManager.getInstance().getResourceFilePath("drivers//IEDriverServer.exe")); 
        	InternetExplorerOptions options = new InternetExplorerOptions();
        	options.setCapability("ignoreZoomSetting", true);
        	driver = new InternetExplorerDriver(options);
    		break;
        }

        if(config.getBrowserWindowSize()) driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(config.getImplicitlyWait(), TimeUnit.SECONDS);
		return driver;
	}	

	public void closeDriver() {
		driver.close();
		driver.quit();
		driver = null;
	}

}