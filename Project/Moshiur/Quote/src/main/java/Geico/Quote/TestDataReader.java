package Geico.Quote;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TestDataReader {
	XSSFWorkbook _workbook;
	// XSSFSheet _loginSheet;
	
	public TestDataReader(File file) { 
		try {
			_workbook = new XSSFWorkbook(new FileInputStream(file));
		} catch (Exception ex) {
			System.out.println("File read error: " + file.getPath());
			System.out.println(ex);
		}
		// _loginSheet = _workbook.getSheet("Logins");
	}
	
	public String getCustomerInfo(String key) { 
		XSSFSheet sheet = _workbook.getSheet("Customer");
		int rowCount = sheet.getLastRowNum();
		
		for	(int i = 0; i < rowCount; i++) { 
			String currentKey = sheet.getRow(i).getCell(0).getStringCellValue();
			if (currentKey.equals(key)) { 
				sheet.getRow(i).getCell(1).setCellType(CellType.STRING);
				String value = sheet.getRow(i).getCell(1).getStringCellValue();
				return value;
			}
		}
		return "";
	}
}

