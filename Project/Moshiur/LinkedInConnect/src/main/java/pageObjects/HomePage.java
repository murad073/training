package pageObjects;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.TestContext;
import selenium.Utils;
import selenium.Wait;

public class HomePage extends BasePage{
	
	public HomePage(TestContext context) {
		super(context);
	}
	
	public boolean IsInHomePage() throws Exception {
		// Utils.waitForElementDisplayed(_nav_item_Profile, 2);
		return _nav_item_Profile.isDisplayed();
	}
	
}

