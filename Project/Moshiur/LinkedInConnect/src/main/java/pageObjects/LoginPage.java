package pageObjects;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.TestContext;
import dataProviders.ConfigFileReader;
import managers.FileReaderManager;
import selenium.Wait;

public class LoginPage extends BasePage {

	public LoginPage(TestContext context) {
		super(context);
		// this.driver = context.getWebDriverManager().getDriver();
		// PageFactory.initElements(driver, this);
		// _context = context;
	}

	@FindBy(how = How.ID, using = "login-email")
	private WebElement txt_Email;

	@FindBy(how = How.ID, using = "login-password")
	private WebElement txt_Password;

	@FindBy(how = How.ID, using = "login-submit")
	private WebElement btn_Submit;

	public HomePage Submit() {
		btn_Submit.click();
		return super._context.getPageObjectManager().getHomePage();
	}

	public void Type_Email(String emailAddress) {
		txt_Email.clear();
		txt_Email.sendKeys(emailAddress);
	}

	public void Type_Password(String password) {
		txt_Password.clear();
		txt_Password.sendKeys(password);
	}

}
