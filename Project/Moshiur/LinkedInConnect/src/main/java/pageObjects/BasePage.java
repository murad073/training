package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.TestContext;
import managers.FileReaderManager;

public class BasePage {

	protected WebDriver _driver;
	protected TestContext _context;
	protected String _baseUrl;

	public BasePage(TestContext context) {
		this._driver = context.getWebDriverManager().getDriver();
		PageFactory.initElements(this._driver, this);
		this._context = context;
		this._baseUrl = FileReaderManager.getInstance().getConfigReader().getApplicationUrl();
	}
	
	@FindBy(how = How.ID, using = "mynetwork-tab-icon")
	protected WebElement _nav_item_MyNetwork;
	
	@FindBy(how = How.ID, using = "feed-tab-icon")
	protected WebElement _nav_item_Home;
	
	@FindBy(how = How.ID, using = "profile-nav-item") 
	protected WebElement _nav_item_Profile;
	
	@FindBy(how = How.ID, using = "nav-typeahead-wormhole") 
	protected WebElement _nav_item_Searchbar;

	public HomePage NavigateToHome() {
		this._nav_item_Home.click();
		return this._context.getPageObjectManager().getHomePage();
	}

	public LoginPage NavigateToLogin() {
		this._driver.navigate().to(this._baseUrl);
		return this._context.getPageObjectManager().getLoginPage();
	}
	
	public MyNetworkPage NavigateToMyNetwork() { 
		this._nav_item_MyNetwork.click();
		return this._context.getPageObjectManager().getMyNetworkPage();
	}

	public PeopleSearchPage NavigateToPeopleSearch() { 
		this._driver.navigate().to(this._baseUrl + "search/results/people/");
		return this._context.getPageObjectManager().getPeopleSearchPage();
	}

	
}
