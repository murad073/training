package pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.TestContext;
import managers.FileReaderManager;
import selenium.Utils;
import selenium.Wait;

public class PeopleSearchPage extends BasePage {

	private int _maxLimit;

	public PeopleSearchPage(TestContext context) {
		super(context);
		this._maxLimit = FileReaderManager.getInstance().getConfigReader().getconnectPageLimit();
	}

	@FindBy(how = How.XPATH, using = "//li[@class='page-list']/ol/li[@class='active']")
	private WebElement lnk_currentPageElement;

	private WebElement getNextPageElement() {
		int currentPageNo = getCurrentPageNo();
		WebElement nextPageElement = Utils.WaitFindElement(super._driver,
				By.xpath("//li[@class='page-list']/ol/li/button[text()='" + (currentPageNo + 1) + "']"));
		return nextPageElement;
	}

	private int getCurrentPageNo() {
		return Integer.parseInt(this.lnk_currentPageElement.getText());
	}

	public boolean HasNextPage() {
		WebElement nextPageElement = getNextPageElement();
		return nextPageElement != null && nextPageElement.isDisplayed() && getCurrentPageNo() < _maxLimit;
	}

	public void GoToNextPage() {
		WebElement element = getNextPageElement();
		Utils.WaitScrollClick(element);
		Wait.wait(2);
	}

	public void ClickAllConnectButton() {
		WebElement connectButton = null;
		By by = By.cssSelector("li.search-result div.ember-view button");
		List<WebElement> elements = super._driver.findElements(by);
		int index = 0;
		if (elements != null && elements.size() > 0) {
			do {
				//WebElement li = elements.get(index);
				//Utils.ScrollToView(li);
				try {
					connectButton  = elements.get(index);
					//connectButton = li.findElement(By.cssSelector("div.ember-view button"));
					String buttonText = connectButton.getText();
					if (buttonText == "Connect") {
						Utils.WaitScrollClick(connectButton);
						Wait.wait(1);
						TryCloseInvitationSentDialog();
					}
				} catch (Exception ex) {
					System.out.println(ex);
				}
				index++;
			} while (index < elements.size());
		}
	}

	public void TryCloseInvitationSentDialog() {
		WebElement doneButton = Utils.WaitFindElement(super._driver,
				By.xpath("//div[@role='dialog']//button[text()='Done']"), 2);
		if (doneButton != null && doneButton.isDisplayed()) {
			doneButton.click();
			Wait.wait(1);
		}
	}
}
