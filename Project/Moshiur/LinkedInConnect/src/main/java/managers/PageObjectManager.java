package managers;

import org.openqa.selenium.WebDriver;

import cucumber.TestContext;
import pageObjects.*;

public class PageObjectManager {
	private LoginPage _loginPage;
	private HomePage _homePage;
	private MyNetworkPage _myNetworkPage;
	private PeopleSearchPage _peopleSearchPage;
	TestContext _context;

	public PageObjectManager(TestContext context) {
		_context = context;
	}

	public LoginPage getLoginPage() {
		return (this._loginPage == null) ? this._loginPage = new LoginPage(this._context) : this._loginPage;
	}

	public HomePage getHomePage() {
		return (this._homePage == null) ? this._homePage = new HomePage(this._context) : this._homePage;
	}

	public MyNetworkPage getMyNetworkPage() {
		return (this._myNetworkPage == null) ? this._myNetworkPage = new MyNetworkPage(this._context) : this._myNetworkPage;
	}

	public PeopleSearchPage getPeopleSearchPage() {
		return (this._peopleSearchPage == null) ? this._peopleSearchPage = new PeopleSearchPage(this._context) : this._peopleSearchPage;
	}
	
	
}

