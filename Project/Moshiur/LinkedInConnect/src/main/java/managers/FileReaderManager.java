package managers;

import java.io.File;

import dataProviders.ConfigFileReader;

public class FileReaderManager {

	private static FileReaderManager fileReaderManager = new FileReaderManager();
	private static ConfigFileReader configFileReader;

	private FileReaderManager() {
	}

	public static FileReaderManager getInstance() {
		return fileReaderManager;
	}

	public ConfigFileReader getConfigReader() {
		String configFilePath = getResourceFilePath("configs//Configuration.properties");
		return (configFileReader == null) ? new ConfigFileReader(configFilePath) : configFileReader;
	}

	public File getResourceFile(String resourceName) {
		ClassLoader loader = this.getClass().getClassLoader();
		File file = new File(loader.getResource(resourceName).getFile());
		return file;
	}
	
	public String getResourceFilePath(String resourceName) {
		ClassLoader loader = this.getClass().getClassLoader();
		File file = new File(loader.getResource(resourceName).getFile());
		return file.getPath();
	}

	public String getReportFilePath() { 
		return getResourceFilePath("configs//extent-config.xml");
	}
}

