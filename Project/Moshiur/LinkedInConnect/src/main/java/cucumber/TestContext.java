package cucumber;

import dataProviders.TestDataReader;
import managers.PageObjectManager;
import managers.WebDriverManager;

public class TestContext {
	private WebDriverManager webDriverManager;
	private PageObjectManager pageObjectManager;
	private TestDataReader testDataReader;

	public TestContext() {
		webDriverManager = new WebDriverManager();
		pageObjectManager = new PageObjectManager(this);
		testDataReader = new TestDataReader(this);
	}

	public WebDriverManager getWebDriverManager() {
		return webDriverManager;
	}

	public PageObjectManager getPageObjectManager() {
		return pageObjectManager;
	}
	
	public TestDataReader getTestDataReader() { 
		return testDataReader;
	}

}
