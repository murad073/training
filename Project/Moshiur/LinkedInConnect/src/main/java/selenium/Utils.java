package selenium;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {

	public static WebElement WaitScrollClick(WebDriver driver, By by) {
		WaitFindElement(driver, by);
		WebElement element = ScrollToView(driver, by);
		element.click();
		Wait.wait(0.5);
		return element;
	}

	public static WebElement WaitScrollClick(WebElement element) {
		ScrollToView(element);
		element.click();
		Wait.wait(0.5);
		return element;
	}

	public static WebElement WaitFindElement(WebDriver driver, By by) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
		return element;
	}

	public static WebElement WaitFindElement(WebDriver driver, By by, int seconds) {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
		return element;
	}

	public static WebElement ScrollToView(WebDriver driver, By by) {
		WebElement element = WaitFindElement(driver, by);
		return ScrollToView(driver, element);
	}

	public static WebElement ScrollToView(WebElement element) {		
		WebDriver driver = ((WrapsDriver)element).getWrappedDriver();
		return ScrollToView(driver, element);
	}

	private static WebElement ScrollToView(WebDriver driver, WebElement element) {		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		Wait.wait(0.2);
		// Actions action = new Actions(driver);
		// action.moveToElement(element);
		// action.perform();
		// Wait.wait(0.2);
		return element;
	}

	// a method for waiting until an element is displayed
	// public static void waitForElementDisplayed(WebDriver driver, By by) throws
	// Exception {
	// waitForElementDisplayed(driver.findElement(by), 5);
	// }
	//
	// public static void waitForElementDisplayed(WebDriver driver, By by, int
	// seconds) throws Exception {
	// waitForElementDisplayed(driver.findElement(by), seconds);
	// }
	//
	// public static void waitForElementDisplayed(WebElement element) throws
	// Exception {
	// waitForElementDisplayed(element, 5);
	// }
	//
	// public static void waitForElementDisplayed(WebElement element, int seconds) {
	// try {
	// // wait for up to XX seconds for our error message
	// long end = System.currentTimeMillis() + (seconds * 1000);
	// while (System.currentTimeMillis() < end) {
	// // If results have been returned, the results are displayed in a drop down.
	// if (element.isDisplayed()) {
	// break;
	// }
	// }
	// } catch (Exception ex) {
	//
	// }
	// }

	//////////////////////////////////////
	// checking element functionality
	//////////////////////////////////////

	// a method for checking id an element is displayed
	// public static void checkElementDisplayed(WebDriver driver, By by) throws
	// Exception {
	// checkElementDisplayed(driver.findElement(by));
	// }
	//
	// public static void checkElementDisplayed(WebElement element) throws Exception
	// {
	// assertTrue(element.isDisplayed());
	// }

	/////////////////////////////////////
	// selenium actions functionality
	/////////////////////////////////////

	// our generic selenium click functionality implemented
	// public static void click(Locators locator, String element) throws Exception {
	// click(getWebElement(locator, element));
	// }
	//
	// public static void click(WebElement element) {
	// Actions selAction = new Actions(driver);
	// selAction.click(element).perform();
	// }
	//
	// // a method to simulate the mouse hovering over an element
	// public static void hover(Locators locator, String element) throws Exception {
	// hover(getWebElement(locator, element));
	// }
	//
	// public static void hover(WebElement element) throws Exception {
	// Actions selAction = new Actions(driver);
	// selAction.moveToElement(element).perform();
	// }
	//
	// // our generic selenium type functionality
	// public static void type(Locators locator, String element, String text) throws
	// Exception {
	// type(getWebElement(locator, element), text);
	// }
	//
	// public static void type(WebElement element, String text) {
	// Actions selAction = new Actions(driver);
	// selAction.sendKeys(element, text).perform();
	// }

	////////////////////////////////////
	// extra base selenium functionality
	////////////////////////////////////

	// a method to grab the web element using selenium webdriver
	// public static WebElement getWebElement( Locators locator, String element )
	// throws Exception {
	// By byElement;
	// switch ( locator ) { //determine which locator item we are interested in
	// case xpath: { byElement = By.xpath(element); break; }
	// case id: { byElement = By.id(element); break; }
	// case name: { byElement = By.name(element); break; }
	// case classname: { byElement = By.className(element); break; }
	// case linktext: { byElement = By.linkText(element); break; }
	// case paritallinktext: { byElement = By.partialLinkText(element); break; }
	// case tagname: { byElement = By.tagName(element); break; }
	// default: { throws new Exception(); }
	// }
	// WebElement query = driver.findElement( byElement ); //grab our element based
	// on the locator
	// return query; //return our query
	// }

	// a method to obtain screenshots
	public static void takeScreenshot(WebDriver driver, String action) throws IOException {
		// make our screenshot name friendly
		action = action.replaceAll("[^a-zA-Z0-9]", "");

		// take a screenshot
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		FileUtils.copyFile(scrFile, new File("target/" + System.currentTimeMillis() + action + ".png"));
	}

}
