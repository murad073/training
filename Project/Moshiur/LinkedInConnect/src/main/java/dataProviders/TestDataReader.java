package dataProviders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import cucumber.TestContext;
import managers.FileReaderManager;

public class TestDataReader {
	TestContext _context;
	XSSFWorkbook _workbook;
	XSSFSheet _loginSheet;
	
	public TestDataReader(TestContext context) { 
		_context = context;
		File file = FileReaderManager.getInstance().getResourceFile("data//TestData.xlsx");
		try {
			_workbook = new XSSFWorkbook(new FileInputStream(file));
		} catch (Exception ex) {
			System.out.println("File read error: " + file.getPath());
			System.out.println(ex);
		}
		_loginSheet = _workbook.getSheet("Logins");
	}
	
	public String getUserName() {
		return _loginSheet.getRow(1).getCell(0).getStringCellValue();
	}
	
	public String getPassword() { 
		return _loginSheet.getRow(1).getCell(1).getStringCellValue();
	}
}
