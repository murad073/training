package stepDefinitions;

import static org.testng.Assert.assertTrue;

import cucumber.TestContext;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dataProviders.TestDataReader;
import managers.PageObjectManager;
import pageObjects.HomePage;
import pageObjects.LoginPage;
import pageObjects.MyNetworkPage;
import pageObjects.PeopleSearchPage;
import selenium.Wait;

public class Steps {

	TestContext _context;
	// LoginPage loginPage;
	// HomePage homePage;
	PageObjectManager _pages;
	// MyNetworkPage networkPage;
	TestDataReader _testData;

	public Steps(TestContext context) {
		this._context = context;
		this._pages = _context.getPageObjectManager();
		// loginPage = pages.getLoginPage();
		this._testData = context.getTestDataReader();
	}

	@Given("^User navigated to login page$")
	public void Navigate_to_login_page() throws Throwable {
		this._pages.getLoginPage().NavigateToLogin();
	}

	@When("^User entered email and password$")
	public void Enter_username_password() throws Throwable {
		this._pages.getLoginPage().Type_Email(_testData.getUserName());
		this._pages.getLoginPage().Type_Password(_testData.getPassword());
	}

	@And("^Click the Sign In button$")
	public void Click_Sign_in_button() throws Throwable {
		this._pages.getLoginPage().Submit();
	}

	@Then("^User should be logged in to his account$")
	@Given("^User logged in to his account$")
	public void Check_account_home_page() throws Throwable {
		assertTrue(this._pages.getHomePage().IsInHomePage());
	}

	@And("^Navigated to the My Network page$")
	public void navigated_to_the_My_Network_page() throws Throwable {
		this._pages.getHomePage().NavigateToMyNetwork();
	}

	@When("^Pending invitations are available$")
	public void there_are_any_pending_invitation_available() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@Then("^Accept one invitation at a time$")
	public void accept_one_invitation_at_a_time() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@Then("^wait for max (\\d+) seconds so that connection suggestion could appear$")
	public void wait_for_max_seconds_so_that_connection_suggestion_could_appear(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@Then("^add all those suggested connect to my profile$")
	public void add_all_those_suggested_connect_to_my_profile() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@And("^Navigated to people search page$")
	public void navigated_to_people_search_page() throws Throwable {
		this._pages.getHomePage().NavigateToPeopleSearch();
	}

	@When("^Connect suggestions are available$")
	public void Connect_suggestions_available() throws Throwable {

	}

	@Then("^Connect all possible suggestions$")
	public void connect_all_possible_suggestions() throws Throwable {
		PeopleSearchPage page = this._pages.getHomePage().NavigateToPeopleSearch();
		
		do {
			Wait.wait(1);
			page.ClickAllConnectButton();
			Wait.wait(1);
			page.ClickAllConnectButton();			
			Wait.wait(1);
			page.GoToNextPage();
		} while (page.HasNextPage());
	}
}
