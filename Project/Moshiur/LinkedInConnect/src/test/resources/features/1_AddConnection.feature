Feature: Add linkedin connections 
	As the acount owner 
  I want to increase my professional connect 
  So that it enrich my profile

Scenario: Login to profile 
	Given User navigated to login page 
	When User entered email and password 
	And Click the Sign In button 
	Then User should be logged in to his account 
	
Scenario: Accept all invitations in My Network page 
	Given User logged in to his account 
	And Navigated to the My Network page 
	When Pending invitations are available 
	Then Accept all pending invitations along with connect suggestion 
	
Scenario: Add people from search result 
	Given User logged in to his account 
	And Navigated to people search page 
	When Connect suggestions are available 
	Then Connect all possible suggestions 
    