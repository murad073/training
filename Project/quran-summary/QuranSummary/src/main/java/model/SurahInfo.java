package model;

public class SurahInfo {
	public int SurahNo;
	public KeyValuePair Name;
	public KeyValuePair Meaning;
	public KeyValuePair Type;
	public KeyValuePair TotalAyah;
	public KeyValuePair Topics;

	@Override
	public String toString() {

		/*
		 * Name of the surah: Al-Ma'idah 
		 * Meaning of the name: The Table Spread Type:
		 * Madani No of Ayah: 120 
		 * Topics: The sura's topics include animals which are
		 * forbidden, Isa (Jesus') and Musa's (Moses) missions.
		 */

		StringBuilder sb = new StringBuilder();
		if (this.Name != null) {
			sb.append(this.Name.Key + ": " + this.Name.Value + System.lineSeparator());
		}
		if (this.Meaning != null) {
			sb.append(this.Meaning.Key + ": " + this.Meaning.Value + System.lineSeparator());
		}
		if (this.Type != null) {
			sb.append(this.Type.Key + ": " + this.Type.Value + System.lineSeparator());
		}
		if (this.TotalAyah != null) {
			sb.append(this.TotalAyah.Key + ": " + this.TotalAyah.Value + System.lineSeparator());
		}
		if (this.Topics != null) {
			sb.append(this.Topics.Key + ": " + this.Topics.Value + System.lineSeparator());
		}
		
		return sb.toString();
	}
}

