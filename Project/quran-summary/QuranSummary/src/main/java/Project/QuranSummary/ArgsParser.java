package Project.QuranSummary;

public class ArgsParser {

	public int surahNumber;
	public String language;
	
	public void Parse(String[] args) throws Exception {
		
		if (args == null || args.length != 2) { 
			throw new InvalidArgumentException();
		}
		
		surahNumber = Integer.parseInt(args[0]);
		language = args[1];
	}
}
