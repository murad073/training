package core;

import model.SurahInfo;

public class Manager {
	private int _surahNo;
	private String _language;
	private IContentReader _reader;
	private IJsonParser _parser;
	private IOut _out;

	public Manager(int surahNo, String language, IContentReader reader, IJsonParser parser, IOut out) {
		this._surahNo = surahNo;
		this._language = language;
		this._reader = reader;
		this._parser = parser;
		this._out = out;
	}

	public void showResult() {

		// 1. read the json file as array of object(Model)
		// 1.1 Read content from resource folder
		String jsonText = this._reader.Read(this._language);

		// 1.2 parse the json as array of Model
		SurahInfo[] surahs = this._parser.Parse(jsonText);

		// 2. from the array of object, search for the right surah number
		SurahInfo foundSurah = null;
		for (int i = 0; i < surahs.length; i++) {
			if (surahs[i].SurahNo == this._surahNo) {
				foundSurah = surahs[i];
				break;
			}
		}

		// 3. show the output
		this._out.Show(foundSurah);
	}
}
