package core;

import model.SurahInfo;

public interface IJsonParser {
	public SurahInfo[] Parse(String jsonString);
}

