package implementations;

import java.io.*;
import com.sendgrid.*;
import core.IOut;
import model.SurahInfo;

public class EmailOut implements IOut {

	private String _toAddress;
	
	public EmailOut(String toAddress) { 
		this._toAddress = toAddress;
	}

	@Override
	public void Show(SurahInfo surah) {
		Email from = new Email("javaselenium18@gmail.com");
		String subject = "Surah info for the surah number: " + surah.SurahNo;
		Email to = new Email(this._toAddress);
				
		Content content = new Content("text/html", surah.toString().replaceAll(System.lineSeparator(), "<br />")); 
		
		Mail mail = new Mail(from, subject, to, content);

		SendGrid sg = new SendGrid("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");
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
			System.out.println(response.getStatusCode());
			System.out.println(response.getBody());
			System.out.println(response.getHeaders());
		} catch (IOException ex) {
		}
	}
}
