package implementations;

import core.IOut;
import model.SurahInfo;

public class ConsoleOut implements IOut {

	@Override
	public void Show(SurahInfo surah) {
		System.out.println("Showing output: ");
		System.out.println(surah.Name.Key + ": " + surah.Name.Value);
		System.out.println(surah.Meaning.Key + ": " + surah.Meaning.Value);
		System.out.println(surah.Type.Key + ": " + surah.Type.Value);
		System.out.println(surah.TotalAyah.Key + ": " + surah.TotalAyah.Value);
		System.out.println(surah.Topics.Key + ": " + surah.Topics.Value);
	}

}
