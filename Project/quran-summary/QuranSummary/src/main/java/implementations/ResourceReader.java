package implementations;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import core.IContentReader;

public class ResourceReader implements IContentReader {

	@Override
	public String Read(String language) { 

		ClassLoader loader = this.getClass().getClassLoader();
		File file = new File(loader.getResource(language + ".json").getFile());
		
		String jsonText = "";
		try {
			jsonText = new String(Files.readAllBytes(file.toPath()));
		} catch (Exception ex) {
		}

		return jsonText;
	}
}
