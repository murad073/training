package implementations;

import com.fasterxml.jackson.databind.ObjectMapper;

import core.IJsonParser;
import model.SurahInfo;

public class JaksonParser implements IJsonParser {

	@Override
	public SurahInfo[] Parse(String jsonString) {
		
		ObjectMapper objectMapper = new ObjectMapper();
		SurahInfo[] surahs = null;
		try {
			surahs = objectMapper.readValue(jsonString, SurahInfo[].class);
		} catch (Exception e) {
		}  
		
		return surahs;
	}
}
